package TestCase;
import PageObject.NudgeObject.Announcement2NudgeObject;
import PageObject.NudgeObject.Announcement3NudgeObject;
import PageObject.NudgeObject.Announcement4NudgeObject;
import PageObject.NudgeObject.Announcement5NudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.NudgeMap;
import Utility.Set_Up;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public class TestCase1_WelcomeNudge extends ExtentReport {
    public static AndroidDriver driver;
    @BeforeTest
    public static void open_app() throws IOException, InterruptedException, ConnectError {
        Set_Up su = new Set_Up();
        su.setupAppium();
        driver = Set_Up.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");

    }
   /* @Test(priority = 1)
    @Description("Start A Journey for Announcement nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/announcementNudgeJourney.json", "src/test/jsonFile/createAnnouncementJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }*/

    @Test(description = "Announcement 1 nudge testCase", priority = 2)
    @Description("Announcement 1 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void welcomeNudge() throws InterruptedException, Exception{
        try {
            WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
            //welcomeNudge.app_backGround();
            //wno.app_foreGround();
            welcomeNudge.miltonGreenBtn_click();
            Thread.sleep(3000);
            welcomeNudge.pointClickBtn_click();
            Thread.sleep(3000);
            welcomeNudge.fireEventBtn_click();
            Thread.sleep(3000);
            welcomeNudge.getWelcomeLog();
            Thread.sleep(1000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Announcement - 1");
            System.out.println(nudgeMap);
            Thread.sleep(3000);
            welcomeNudge.WelcomeNudgeScreenShot();
            Thread.sleep(3000);
            welcomeNudge.nudgeMatch();
            Thread.sleep(3000);
            welcomeNudge.welcomeText();
            Thread.sleep(1000);
            welcomeNudge.label2Text();
            Thread.sleep(1000);
            welcomeNudge.oKGotItBtnText();
            welcomeNudge.okGotItBtn_click();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
   /* @Test(priority = 3)
    @Description("Announcement-2 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement2() throws Exception {
        Announcement2NudgeObject announcement2 = new Announcement2NudgeObject(driver);
        announcement2.announcement2NudgeScreenShot();
        Thread.sleep(3000);
        announcement2.nudgeMatch();
        announcement2.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement2.getAnnouncement2Log();
        Thread.sleep(3000);
        announcement2.getLabel1Text();
        Thread.sleep(3000);
        announcement2.getBtn1Text();
        Thread.sleep(3000);
        announcement2.clickBtn1();
        Thread.sleep(3000);

    }
    @Test(priority = 4)
    @Description("Announcement-3 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement3() throws Exception {
        Announcement3NudgeObject announcement3 = new Announcement3NudgeObject(driver);
        announcement3.announcement3NudgeScreenShot();
        Thread.sleep(3000);
        announcement3.nudgeMatch();
        announcement3.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement3.getAnnouncement3Log();
        Thread.sleep(3000);
        announcement3.getLabel0Text();
        Thread.sleep(3000);
        announcement3.getBtn1Text();
        Thread.sleep(3000);
        announcement3.clickBtn1();
        Thread.sleep(3000);

    }
    @Test(priority = 5)
    @Description("Announcement-4 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement4() throws Exception {
        Announcement4NudgeObject announcement4 = new Announcement4NudgeObject(driver);
        announcement4.announcement4NudgeScreenShot();
        Thread.sleep(3000);
        announcement4.nudgeMatch();
        announcement4.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement4.getAnnouncement4Log();
        Thread.sleep(3000);
        announcement4.getLabel1Text();
        Thread.sleep(3000);
        announcement4.getBtn1Text();
        Thread.sleep(3000);
        announcement4.clickBtn1();
        Thread.sleep(3000);

    }
    @Test(priority = 6)
    @Description("Announcement-5 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement5() throws Exception {
        Announcement5NudgeObject announcement5 = new Announcement5NudgeObject(driver);
        announcement5.announcement5NudgeScreenShot();
        Thread.sleep(3000);
        announcement5.nudgeMatch();
        announcement5.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement5.getAnnouncement5Log();
        Thread.sleep(3000);
        announcement5.getLabel1Text();
        Thread.sleep(3000);
        announcement5.getBtn1Text();
        Thread.sleep(3000);
        announcement5.clickBtn1();
        Thread.sleep(3000);

    }
    @Step("Delete Announcement nudge journey")
    @AfterTest
    public  void afterTest()  {
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();
        driver.quit();

    }*/

}
