package TestCase;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.Alert1NudgeObject;
import PageObject.NudgeObject.Alert2NudgeObject;
import PageObject.NudgeObject.Promotion2NudgeObject;
import PageObject.NudgeObject.Promotion4NudgeObject;
import Utility.ExtentReport;
import Utility.Set_Up;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase5_PromotionNudge extends ExtentReport {
    //public static AndroidDriver driver;
    @BeforeTest
    public static void open_app() throws IOException, InterruptedException, ConnectError {
        Set_Up su = new Set_Up();
        su.setupAppium();
        driver = Set_Up.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");
    }
    @Test(priority = 0)
    @Step("Start A Journey for Promotion nudges")
    @Description("Start A Journey for Promotion nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/promotionNudgeJourney.json", "src/test/jsonFile/createPromotionJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(description = "Promotion 4 nudge testCase", priority = 1)
    @Description("Promotion 4 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert1Nudge() throws InterruptedException, Exception {
        try {
            Promotion4NudgeObject promotion4 = new Promotion4NudgeObject(driver);
            promotion4.app_backGround();
            Thread.sleep(3000);
            promotion4.miltonGreenBtn_click();
            Thread.sleep(3000);
            promotion4.pointClickBtn_click();
            Thread.sleep(3000);
            promotion4.fireEventBtn_click();
            Thread.sleep(3000);
            promotion4.getLabel1Text();
            Thread.sleep(3000);
            promotion4.getLabe2Text();
            Thread.sleep(3000);
            promotion4.getBtn1Text();
            Thread.sleep(3000);
            promotion4.getBtn2Text();
            Thread.sleep(3000);
            promotion4.promotion4NudgeObjectScreenShot();
            Thread.sleep(3000);
            promotion4.nudgeMatch();
            Thread.sleep(3000);
            promotion4.clickBtn2();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Test(description = "Promotion 2 nudge testCase", priority = 1)
    @Description("Promotion 2 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert2Nudge() throws InterruptedException, IOException {
        try {
            Promotion2NudgeObject promotion2 = new Promotion2NudgeObject(driver);
            promotion2.getLabelHText();
            Thread.sleep(3000);
            promotion2.getImgHText();
            Thread.sleep(3000);
            promotion2.getBtn1Text();
            Thread.sleep(3000);
            promotion2.getBtn2Text();
            Thread.sleep(3000);
            promotion2.promotion2NudgeObjectScreenShot();
            Thread.sleep(3000);
            promotion2.nudgeMatch();
            Thread.sleep(3000);
            promotion2.clickBtn2();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete Promotion nudge journey")
    @AfterTest
    public static void tearDown(){
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();
        driver.quit();
    }
}
