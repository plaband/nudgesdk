package TestCase;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.NpsNudgeObject;
import PageObject.NudgeObject.UserFeedbackNudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.Set_Up;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestCase2_FeedbackNudge extends ExtentReport {
    //public static AndroidDriver driver;
    @BeforeTest
    public static void open_app() throws IOException, InterruptedException, ConnectError {
        Set_Up su = new Set_Up();
        su.setupAppium();
        driver = Set_Up.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");

    }
    @Test(priority = 0)
    @Description("Start A Journey for FeedBack nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/feedbackNudgeJourney.json", "src/test/jsonFile/createFeedbackJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(description = "Net Promoter Score nudge testCase", priority = 1)
    @Description("Net Promoter Score nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void netPromoterScoreNudge() throws InterruptedException, Exception {
        try {
            NpsNudgeObject netPromoterScore = new NpsNudgeObject(driver);
            netPromoterScore.app_backGround();
            Thread.sleep(3000);
            netPromoterScore.miltonGreenBtn_click();
            Thread.sleep(3000);
            netPromoterScore.pointClickBtn_click();
            Thread.sleep(3000);
            netPromoterScore.fireEventBtn_click();
            Thread.sleep(3000);
            netPromoterScore.getLabel1Text();
            Thread.sleep(1000);
            netPromoterScore.getLabel3Text();
            Thread.sleep(1000);
            netPromoterScore.getNpsLayout();
            Thread.sleep(1000);
            netPromoterScore.getLeastLikelyText();
            Thread.sleep(1000);
            netPromoterScore.getVeryLikelyText();
            netPromoterScore.NpsNudgeScreenShot();
            Thread.sleep(3000);
            netPromoterScore.nudgeMatch();
            Thread.sleep(3000);
            netPromoterScore.getBtn1Text();
            Thread.sleep(1000);
            netPromoterScore.selectRating();
            Thread.sleep(3000);
            netPromoterScore.btn1Click();
            Thread.sleep(2000);

        }catch (Exception e){
            System.out.println(e);
        }
    }
    @Test(description = "User FeedBack nudge testCase", priority = 2)
    @Description("User FeedBack nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void userFeedBackNudge() throws Exception {
        UserFeedbackNudgeObject userFeedBack = new UserFeedbackNudgeObject(driver);
        userFeedBack.getLabel1Text();
        Thread.sleep(1000);
        userFeedBack.getLabel2Text();
        Thread.sleep(1000);
        userFeedBack.getTextView();
        Thread.sleep(1000);
        userFeedBack.getCancelBtn();
        Thread.sleep(1000);
        userFeedBack.getOkBtn();
        Thread.sleep(1000);
        userFeedBack.UserFeedbackNudgeScreenShot();
        Thread.sleep(3000);
        userFeedBack.nudgeMatch();
        Thread.sleep(3000);
        userFeedBack.enterTextView("Hi");
        Thread.sleep(2000);
        userFeedBack.okBtnClick();

    }
    @Step("Delete FeedBack nudge journey")
    @AfterTest
    public static  void  tearDown(){
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();
        driver.quit();
    }

}
