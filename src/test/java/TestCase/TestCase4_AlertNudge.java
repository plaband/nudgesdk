package TestCase;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.Action1NudgeObject;
import PageObject.NudgeObject.Alert1NudgeObject;
import PageObject.NudgeObject.Alert2NudgeObject;
import Utility.ExtentReport;
import Utility.Set_Up;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase4_AlertNudge extends ExtentReport {

    //public static AndroidDriver driver;
    @BeforeTest
    public static void open_app() throws IOException, InterruptedException, ConnectError {
        Set_Up su = new Set_Up();
        su.setupAppium();
        driver = Set_Up.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");
    }
    @Test(priority = 0)
    @Step("Start A Journey for Alert nudges")
    @Description("Start A Journey for Alert nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/alertNudgeJourney.json", "src/test/jsonFile/createAlertJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(description = "Alert 1 nudge testCase", priority = 1)
    @Description("Alert 1 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert1Nudge() throws InterruptedException, Exception {
        try {
            Alert1NudgeObject alert1 = new Alert1NudgeObject(driver);
            alert1.app_backGround();
            Thread.sleep(3000);
            alert1.miltonGreenBtn_click();
            Thread.sleep(3000);
            alert1.pointClickBtn_click();
            Thread.sleep(3000);
            alert1.fireEventBtn_click();
            Thread.sleep(3000);
            alert1.getLabel1Text();
            Thread.sleep(3000);
            alert1.Alert1NudgeObjectScreenShot();
            Thread.sleep(3000);
            alert1.nudgeMatch();
            Thread.sleep(3000);
            alert1.clickBtnX();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Test(description = "Alert 2 nudge testCase", priority = 2)
    @Description("Alert 2 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert2Nudge() throws InterruptedException, IOException {
        try {
            Alert2NudgeObject alert2 = new Alert2NudgeObject(driver);
            alert2.getLabel1Text();
            Thread.sleep(3000);
            alert2.getBtn1Text();
            Thread.sleep(3000);
            alert2.Alert2NudgeObjectScreenShot();
            Thread.sleep(3000);
            alert2.nudgeMatch();
            Thread.sleep(3000);
            alert2.clickBtn1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete Alert nudge journey")
    @AfterTest
    public static void tearDown(){
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();
        driver.quit();
    }

}
