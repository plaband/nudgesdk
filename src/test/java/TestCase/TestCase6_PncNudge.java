package TestCase;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.*;
import Utility.ExtentReport;
import Utility.Set_Up;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase6_PncNudge extends ExtentReport {
    public static AndroidDriver driver;
    @BeforeTest
    public static void open_app() throws IOException, InterruptedException, ConnectError {
        Set_Up su = new Set_Up();
        su.setupAppium();
        driver = Set_Up.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");
    }
    @Test(priority = 0)
    @Step("Start A Journey for PNC nudges")
    @Description("Start A Journey for PNC nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/pncNudgeJourney.json", "src/test/jsonFile/createPncJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(description = "Tooltip - Text, Button nudge testCase", priority = 1)
    @Description("Tooltip - Text, Button nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void toolTipNudge() throws InterruptedException, Exception {
        try {
            TooltipWithTextBtnNudgeObject tooltip = new TooltipWithTextBtnNudgeObject(driver);
            tooltip.app_backGround();
            Thread.sleep(3000);
            tooltip.miltonGreenBtn_click();
            Thread.sleep(3000);
            tooltip.pointClickBtn_click();
            Thread.sleep(3000);
            tooltip.clickAllRepeatable();
            Thread.sleep(3000);
            tooltip.clickListInList();
            Thread.sleep(3000);
            tooltip.fireEventBtn_click();
            Thread.sleep(3000);
            tooltip.getLabel1Text();
            Thread.sleep(3000);
            tooltip.getBtn1Text();
            Thread.sleep(3000);
            tooltip.TooltipWithTextBtnNudgeObjectScreenShot();
            Thread.sleep(3000);
            tooltip.nudgeMatch();
            Thread.sleep(3000);
            tooltip.clickBtn1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Test(description = "Beacon - Text, Button nudge testCase", priority = 2)
    @Description("Beacon - Text, Button nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void beaconNudge() throws InterruptedException, IOException {
        try {
            BeaconWithTextBtnNudgeObject beacon = new BeaconWithTextBtnNudgeObject(driver);
            beacon.getLabel1Text();
            Thread.sleep(3000);
            beacon.getBtn1Text();
            Thread.sleep(3000);
            beacon.BeaconWithTextBtnNudgeObjectScreenShot();
            Thread.sleep(3000);
            beacon.nudgeMatch();
            Thread.sleep(3000);
            beacon.clickBtn1();
            Thread.sleep(3000);
        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Test(description = "Spotlight - Text, Button nudge testCase", priority = 3)
    @Description("Spotlight - Text, Button nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void spotLightNudge() throws InterruptedException, IOException {
        try {
            SpotLightWithTextBtnNudgeObject spotLight = new SpotLightWithTextBtnNudgeObject(driver);
            spotLight.getLabel1Text();
            Thread.sleep(3000);
            spotLight.getBtn1Text();
            Thread.sleep(3000);
            spotLight.SpotLightWithTextBtnNudgeObjectScreenShot();
            Thread.sleep(3000);
            spotLight.nudgeMatch();
            Thread.sleep(3000);
            spotLight.clickBtn1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete PNC nudge journey")
    @AfterTest
    public static void tearDown(){
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();
        driver.quit();
    }
}
