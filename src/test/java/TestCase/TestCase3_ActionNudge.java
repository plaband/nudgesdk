package TestCase;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.Action1NudgeObject;
import PageObject.NudgeObject.Action2NudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.Set_Up;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase3_ActionNudge extends ExtentReport {
    //public static AndroidDriver driver;
    @BeforeTest
    public static void open_app() throws IOException, InterruptedException, ConnectError {
        Set_Up su = new Set_Up();
        su.setupAppium();
        driver = Set_Up.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");

    }
    @Test(priority = 0)
    @Description("Start A Journey for Action nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/actionsNudgeJourney.json", "src/test/jsonFile/createActionsJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }

    @Test(description = "Action 1 nudge testCase", priority = 1)
    @Description("Action 1 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void action1Nudge() throws InterruptedException, Exception {
        try {
            Action1NudgeObject action1 = new Action1NudgeObject(driver);
            action1.app_backGround();
            Thread.sleep(3000);
            action1.miltonGreenBtn_click();
            Thread.sleep(3000);
            action1.pointClickBtn_click();
            Thread.sleep(3000);
            action1.fireEventBtn_click();
            Thread.sleep(3000);
            action1.getLabel1Text();
            Thread.sleep(3000);
            action1.getLabel2Text();
            Thread.sleep(3000);
            action1.getButton1Text();
            Thread.sleep(3000);
            action1.getButton2Text();
            Thread.sleep(3000);
            action1.Action1NudgeObjectScreenShot();
            Thread.sleep(3000);
            action1.nudgeMatch();
            Thread.sleep(3000);
            action1.clickButton1();

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Test(description = "Action 2 nudge testCase", priority = 2)
    @Description("Action 2 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void action2Nudge() throws InterruptedException, IOException {
        try {
            Action2NudgeObject action2 = new Action2NudgeObject(driver);
            action2.getLabel2Text();
            Thread.sleep(3000);
            action2.getLabel2Text();
            Thread.sleep(3000);
            action2.getButton1Text();
            Thread.sleep(3000);
            action2.Action2NudgeObjectScreenShot();
            Thread.sleep(3000);
            action2.nudgeMatch();
            Thread.sleep(3000);
            action2.clickButton1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete action nudge journey")
    @AfterTest
    public static void tearDown(){

        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();
        driver.quit();
    }
}
