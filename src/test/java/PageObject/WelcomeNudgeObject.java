package PageObject;

import Utility.ExtentReport;
import Utility.CompareImages;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.ILoggerFactory;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.Duration;
import java.util.List;
import java.util.Map;


public class WelcomeNudgeObject extends ExtentReport {
    AndroidDriver<?> driver;
    // creates a toggle for the given test, adds all log events under it
    @AndroidFindBy(xpath = "//android.widget.Button[@text='START MILTONGREEN']")
    private WebElement miltonGreenBtn;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='POINT AND CLICK']")
    private WebElement pointClickBtn;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FIRE EVENT']")
    private WebElement fireEventBtn;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement welcomeNudgeScreenShot;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='OK, GOT IT']")
    private WebElement okGotItBtn;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btnX")
    private WebElement crossBtn;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement welcomeText;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label2")
    private WebElement label2Text;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/server_spinner")
    private WebElement envSpinner;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'prod']")
    private WebElement prodEnv;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/appid_spinner")
    private WebElement appIDSpinner;
    @AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text = 'India_Server']")
    private WebElement appName;
    @AndroidFindBy(xpath = "//android.widget.Button[@text = 'OK']")
    private WebElement oKBtnFromPopUp;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/fire_fire_custom_event")
    private WebElement fireCustomEvent;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/vendor_spinner")
    private WebElement vendorSpinner;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text = 'Smartech']")
    private WebElement smartTech;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/name")
    private WebElement eventName;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/save_event_pnc")
    private WebElement savePncEvent;
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
            ".scrollIntoView(new UiSelector().xpath(\"//android.widget.Button[@text = 'FIRE CUSTOM EVENT']\"))")
    private WebElement scrollElement;
    public WelcomeNudgeObject(AndroidDriver<?> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    public WelcomeNudgeObject closeApp(){
        driver.closeApp();
        return this;
    }
    public WelcomeNudgeObject scrollToAnElementByText(AndroidDriver<AndroidElement> driver, String text) {
        driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector())" +
                ".scrollIntoView(new UiSelector().text(\"" + text + "\"));"));
        return this;
    }
    public WelcomeNudgeObject scrollAndClick(String visibleText) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + visibleText +"\").instance(0))").click();
        //driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+visibleText+"\").instance(0))");
            //driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\""+visibleText+"\").instance(0))").click();
        return this;
    }
    public WelcomeNudgeObject scrollTo() {
        //driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+visibleText+"\").instance(0))");
        try {
            driver.findElement(MobileBy.AndroidUIAutomator(
                    "new UiScrollable(new UiSelector().scrollable(true)).scrollToEnd(10)"));
            fireCustomEvent.click();
        } catch (InvalidSelectorException e) {

        }
        return this;
    }
    public WelcomeNudgeObject launchApp(){
        driver.launchApp();
        return this;
    }
    public WelcomeNudgeObject resetApp(){
        driver.resetApp();
        return this;
    }
    public WelcomeNudgeObject terminateApp(){
        driver.terminateApp("io.hansel.sampleapp.userjourney.miltongreen");
        return this;
    }
    public WelcomeNudgeObject envScrollMethod(){
        envSpinner.click();
        prodEnv.click();
        appIDSpinner.click();
        return this;
    }
    public WelcomeNudgeObject appNameSelect(){
        appName.click();
        return this;
    }
    public WelcomeNudgeObject popUpSelect(){
        oKBtnFromPopUp.click();
        return this;
    }
    public WelcomeNudgeObject clickFireCustomEvent(){
        fireCustomEvent.click();
        return this;
    }
    public WelcomeNudgeObject selectVendorSpinner(){
//        WebDriverWait wait = new WebDriverWait(driver, 30);
//        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("io.hansel.sampleapp.userjourney.miltongreen:id/vendor_spinner"))));
        vendorSpinner.click();
        return this;
    }
    public WelcomeNudgeObject selectSmartTech(){
        smartTech.click();
        return this;
    }
    public WelcomeNudgeObject enterEventName(String event){
        eventName.sendKeys(event);
        return this;
    }
    public WelcomeNudgeObject selectSavePncEvent(){
        savePncEvent.click();
        return this;
    }
    public WelcomeNudgeObject pressBackButton(){
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        return this;
    }
    public WelcomeNudgeObject miltonGreenBtn_click() throws Exception {
        File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("target/screenShots/welcomeObject.png"));
            if (miltonGreenBtn.isDisplayed() && miltonGreenBtn.isEnabled()) {
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "miltonGreenBtn", miltonGreenBtn)));
                //test.log(LogStatus.PASS,test.addScreencast(getScreenShot(driver, "miltonGreenBtn2")));
                miltonGreenBtn.click();
                test.log(LogStatus.PASS, "miltonGreenBtn is clicked");
                //logger.log(LogStatus.PASS, logger.addScreenCapture("target/screenShots/welcomeObject.png"));
                //test.log(LogStatus.PASS, logger.addScreenCapture(getScreenShot("miltonGreenBtn")));
                Thread.sleep(1000);

            } else {
                test.log(LogStatus.FAIL,test.addScreenCapture(getFailScreenshot(driver, "failMiltonGreenBtn")));
            }
        return this;
    }

    public WelcomeNudgeObject pointClickBtn_click() throws InterruptedException {
        if (pointClickBtn.isDisplayed() && pointClickBtn.isDisplayed()) {
            pointClickBtn.click();
            test.log(LogStatus.PASS, "pointClickBtn is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        } else {
            takeScreenshot();
            test.log(LogStatus.FAIL, "pointClickBtn is not clicked");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    public WelcomeNudgeObject fireEventBtn_click() throws InterruptedException {
        if (fireEventBtn.isDisplayed() && fireEventBtn.isDisplayed()) {
            fireEventBtn.click();
            test.log(LogStatus.PASS, "Fire Event button is clicked");
            Thread.sleep(1000);
        } else {
            takeScreenshot();
            test.log(LogStatus.FAIL, "Fire Event button is not clicked");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    public void WelcomeNudgeScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer + "_" + deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualWelcomeNudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try {
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualWelcomeNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = welcomeNudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = welcomeNudgeScreenShot.getSize().getWidth();
                int eleHeight = welcomeNudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "welcomeNudgeImage", welcomeNudgeScreenShot)));
                //System.out.println(screenshotLocation.toString());
                Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            } else {
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualWelcomeNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = welcomeNudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = welcomeNudgeScreenShot.getSize().getWidth();
                int eleHeight = welcomeNudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                System.out.println(screenshotLocation.toString());
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "welcomeNudgeImage", welcomeNudgeScreenShot)));
                Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            }

        } catch (IOException e) {
            e.printStackTrace();
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "welcomeNudgeImage", welcomeNudgeScreenShot)));
        }

    }

    /* public void captureScreenshot() {
      File scrFile = ((TakesScreenshot) driver)
              .getScreenshotAs(OutputType.FILE);
      try {

          File temp = new File("target/actualScreenshots/actualWecomeNudge.png");
          boolean exists = temp.exists();
          if (exists == true){
              File scr1File = ((TakesScreenshot) driver)
                      .getScreenshotAs(OutputType.FILE);
              test.log(LogStatus.INFO, "file exists hence deleted");
              temp.delete();
              String filePath = "target/actualScreenshots/actualWecomeNudge.png";
              FileUtils.copyFile(scrFile, new File(filePath));
              test.log(LogStatus.INFO, "file created");
          }else {
              File scr2File = ((TakesScreenshot) driver)
                      .getScreenshotAs(OutputType.FILE);
              String filePath = "target/actualScreenshots/actualWecomeNudge.png";
              FileUtils.copyFile(scrFile, new File(filePath));
              test.log(LogStatus.INFO, "file created");
          }
      } catch (IOException e) {
          e.printStackTrace();
      }
  }*/
    public WelcomeNudgeObject welcomeText() {
        try {
            if (welcomeText.getText().equals("Welcomeu0021")) {
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "welcomeText", welcomeText)));
                test.log(LogStatus.PASS, "welcomeText is displayed");
            } else {
                test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "welcomeText", welcomeText)));
                test.log(LogStatus.FAIL, "welcomeText is not displayed");
            }

        } catch (Exception e) {
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    public WelcomeNudgeObject label2Text() throws Exception {
        if (label2Text.getText().equals("We are pleased to announce our new offering. You can learn more about it at www.example.com")) {
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "welcomeLabel2Text", label2Text)));
            test.log(LogStatus.PASS, "label2Text is displayed");
        } else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "welcomeLabel2Text", label2Text)));
            test.log(LogStatus.FAIL, "label2Text is not displayed");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    public WelcomeNudgeObject oKGotItBtnText() throws Exception {
        if (okGotItBtn.getText().equals("OK, GOT IT")) {
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "welcomeOkGotItBtn", okGotItBtn)));
            test.log(LogStatus.PASS, "OK, GOT IT button text is displayed");
        } else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "welcomeOkGotItBtn", okGotItBtn)));
            test.log(LogStatus.FAIL, "OK, GOT IT button text is not displayed");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }


    public void nudgeMatch() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer + "_" + deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualWelcomeNudge.png";
        System.out.println(file);
        //CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedWecomeNudge.png", file);
        //captureScreenShots();
    }

    public WelcomeNudgeObject okGotItBtn_click() throws Exception {
        if (okGotItBtn.isDisplayed() && okGotItBtn.isDisplayed()) {
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "welcomeOkGotItBtn", okGotItBtn)));
            okGotItBtn.click();
            test.log(LogStatus.PASS, "okGotItBtn is clicked");
            //captureScreenShots();
            Thread.sleep(1000);
        } else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "welcomeOkGotItBtn", okGotItBtn)));
            test.log(LogStatus.FAIL, "okGotItBtn is not clicked");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            //captureScreenShots();
        }
        return this;
    }

    /*public welcomeNudgeObject crossBtn_click() throws InterruptedException{
    if (crossBtn.isDisplayed()&&crossBtn.isDisplayed()){
        crossBtn.click();
        test.log(LogStatus.PASS,"crossBtn is clicked");
        Thread.sleep(1000);
       }
      else {
        test.log(LogStatus.PASS,"crossBtn is not clicked");
       }
       return this;
}*/
    public WelcomeNudgeObject app_backGround() {

        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }

    public WelcomeNudgeObject app_foreGround() {
        driver.runAppInBackground(Duration.ofSeconds(2));
        return this;
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] takeScreenshot() {
        // Take a screenshot as byte array and return
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public WelcomeNudgeObject getWelcomeLog() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.json");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }



    }


