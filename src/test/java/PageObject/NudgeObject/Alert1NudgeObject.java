package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.xml.bind.v2.model.core.ID;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class Alert1NudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement alert1NudgeScreenShot;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/container_btnX")
    private WebElement btnXElement;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='START MILTONGREEN']")
    private WebElement miltonGreenBtn;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='POINT AND CLICK']")
    private WebElement pointClickBtn;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FIRE EVENT']")
    private WebElement fireEventBtn;

    public Alert1NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Get label1 Text of Alert- 1 nudge")
    public Alert1NudgeObject getLabel1Text(){
        if (label1Element.getText().equals("We are currently experiencing a few issues and should be back shortly. Stay tuned!")){
            test.log(LogStatus.PASS, "label1 text is matched");
        }else{
            test.log(LogStatus.FAIL, "label1 Text is not matched");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("BtnX is Clickable or not")
    public Alert1NudgeObject clickBtnX(){
        if (btnXElement.isDisplayed() && btnXElement.isEnabled()){
            btnXElement.click();
            test.log(LogStatus.PASS, "BtnX is clicked");
        }else {
            test.log(LogStatus.FAIL,"BtnX is not clickable");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    @Step("Click MiltonGreen Button")
    public Alert1NudgeObject miltonGreenBtn_click() throws InterruptedException, IOException {
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("target/screenShots/welcomeObject.png"));
        try {
            if (miltonGreenBtn.isDisplayed() && miltonGreenBtn.isEnabled()) {
                miltonGreenBtn.click();
                test.log(LogStatus.PASS, "miltonGreenBtn is clicked");
                test.log(LogStatus.PASS, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
                Thread.sleep(1000);

            } else {
                Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
                test.log(LogStatus.FAIL, "miltonGreenBtn is not clicked");
                test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));

            }
        }catch (Exception e){
            test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;

    }
    @Step("Click on point & Click Btn")
    public Alert1NudgeObject pointClickBtn_click() throws InterruptedException {
        if (pointClickBtn.isDisplayed()&&pointClickBtn.isDisplayed()){
            pointClickBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Click on the Fire Event button")
    public Alert1NudgeObject fireEventBtn_click() throws InterruptedException {
        if (fireEventBtn.isDisplayed()&&fireEventBtn.isDisplayed()){
            fireEventBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    @Step("Take ScreenShots of Alert-1 Nudge")
    public void Alert1NudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert1Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert1Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = alert1NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = alert1NudgeScreenShot.getSize().getWidth();
                int eleHeight = alert1NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "alert1Image", alert1NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert1Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = alert1NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = alert1NudgeScreenShot.getSize().getWidth();
                int eleHeight = alert1NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "alert1Image", alert1NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "alert1Image", alert1NudgeScreenShot)));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            e.printStackTrace();
        }
    }
    @Step("Compare Captured Alert1 screenshots with expected Screenshots")
    public void nudgeMatch() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert1Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedAlert1Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Move app to background for 10 seconds")
    public Alert1NudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Alert-1 nudge")
    public Alert1NudgeObject getAlert1Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] screenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

}
