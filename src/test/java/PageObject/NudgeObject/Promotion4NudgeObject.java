package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class Promotion4NudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement promotion4NudgeScreenShot;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='START MILTONGREEN']")
    private WebElement miltonGreenBtn;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='POINT AND CLICK']")
    private WebElement pointClickBtn;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FIRE EVENT']")
    private WebElement fireEventBtn;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label2")
    private WebElement label2Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn2")
    private WebElement btn2Element;

    public Promotion4NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Click MiltonGreen Button")
    public Promotion4NudgeObject miltonGreenBtn_click() throws InterruptedException, IOException {
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("target/screenShots/welcomeObject.png"));
        try {
            if (miltonGreenBtn.isDisplayed() && miltonGreenBtn.isEnabled()) {
                miltonGreenBtn.click();
                test.log(LogStatus.PASS, "miltonGreenBtn is clicked");
                test.log(LogStatus.PASS, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
                Thread.sleep(1000);

            } else {
                test.log(LogStatus.FAIL, "miltonGreenBtn is not clicked");
                test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));

            }
        }catch (Exception e){
            test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
        }
        return this;

    }
    @Step("Click on point & Click Btn")
    public Promotion4NudgeObject pointClickBtn_click() throws InterruptedException {
        if (pointClickBtn.isDisplayed()&&pointClickBtn.isDisplayed()){
            pointClickBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
        }
        return this;
    }
    @Step("Click on the Fire Event button")
    public Promotion4NudgeObject fireEventBtn_click() throws InterruptedException {
        if (fireEventBtn.isDisplayed()&&fireEventBtn.isDisplayed()){
            fireEventBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
        }
        return this;
    }
    @Step("Get label1 text from promotion4 Nudge")
    public Promotion4NudgeObject getLabel1Text(){
        if (label1Element.getText().equals("You're not giving up coffee, are you?")){
            test.log(LogStatus.PASS, "label1 text is matching");
        }else {
            test.log(LogStatus.FAIL, "label1 text is not matching");
        }
        return this;
    }
    @Step("Get label2 text from promotion4 Nudge")
    public Promotion4NudgeObject getLabe2Text(){
        if (label2Element.getText().equals("You haven't redeemed your 2 expresso yet")){
            test.log(LogStatus.PASS, "label2 text is matching");
        }else {
            test.log(LogStatus.FAIL, "label2 text is not matching");
        }
        return this;
    }
    @Step("Get Btn1 text from promotion4 Nudge")
    public Promotion4NudgeObject getBtn1Text(){
        if (btn1Element.getText().equals("NOT NOW")){
            test.log(LogStatus.PASS, "Btn1 text is matching");
        }else {
            test.log(LogStatus.FAIL, "Btn1 text is not matching");
        }
        return this;
    }
    @Step("Get Btn2 text from promotion4 Nudge")
    public Promotion4NudgeObject getBtn2Text(){
        if (btn2Element.getText().equals("REDEEM")){
            test.log(LogStatus.PASS, "Btn2 text is matching");
        }else {
            test.log(LogStatus.FAIL, "Btn2 text is not matching");
        }
        return this;
    }
    @Step("Check if Btn2 is clicked or not for promotion4 Nudge")
    public Promotion4NudgeObject clickBtn2(){
        if (btn2Element.isDisplayed() && btn2Element.isEnabled()){
            btn1Element.click();
            test.log(LogStatus.PASS, "Btn2 is Clicked");
        }else {
            test.log(LogStatus.FAIL, "Btn2 is not Clicked");
        }
        return this;
    }
    @Step("Take ScreenShots of Promotion4 Nudge")
    public void promotion4NudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion4Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion4Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = promotion4NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = promotion4NudgeScreenShot.getSize().getWidth();
                int eleHeight = promotion4NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "promotion4NudgeImage", promotion4NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion4Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = promotion4NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = promotion4NudgeScreenShot.getSize().getWidth();
                int eleHeight = promotion4NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "promotion4NudgeImage", promotion4NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "promotion4NudgeImage", promotion4NudgeScreenShot)));
        }
    }
    @Step("Compare Captured Promotion4 screenshots with expected Screenshots")
    public void nudgeMatch() throws InterruptedException, Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion4Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedPromotion4Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Move app to background for 10 seconds")
    public Promotion4NudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Promotion4 nudge")
    public Promotion4NudgeObject getPromotion4Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
}
