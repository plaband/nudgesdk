package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Announcement3NudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement announcement3NudgeScreenShot;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label0")
    private WebElement label0Text;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/img1")
    private WebElement announcement3Img;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1;

    public Announcement3NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Attachment(value = "Screenshot", type = "image/png")
    public  byte[] takeScreenshot() {
        // Take a screenshot as byte array and return
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }
    @Step("Image Container Test")
    @Description("Image Container Test")
    public Announcement3NudgeObject getAnnouncement2Img(){
        if (announcement3Img.isDisplayed()){
            test.log(LogStatus.PASS, "Image Container is Visible");
        }else {
            test.log(LogStatus.FAIL, "Image Container is not Visible");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));

        }
        return this;
    }
    @Step("Label0 Text Matching")
    @Description("Label0 Text Checking")
    public Announcement3NudgeObject getLabel0Text(){
        if (label0Text.getText().equals("You can now search for your friend's location...")){
            test.log(LogStatus.PASS, "Label0 Text is found and it is matching");
        }else {
            test.log(LogStatus.FAIL, "Label0 Text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button Text matching")
    @Description("button Text checking")
    public Announcement3NudgeObject getBtn1Text(){
        if (btn1.getText().equals("OK, GOT IT")){
            test.log(LogStatus.PASS, "Button text is matching");
        }else {
            test.log(LogStatus.FAIL, "Button text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button click action checking")
    @Description("Check Button click for Announcement-3 Nudge")
    public Announcement3NudgeObject clickBtn1(){
        if (btn1.isDisplayed() && btn1.isEnabled()){
            btn1.click();
            test.log(LogStatus.PASS, "Button is clicked");
        }else {
            test.log(LogStatus.PASS, "Button is not clicked");
        }
        return this;
    }
    @Step("Take Announcement -3 Nudge ScreenShot")
    public void announcement3NudgeScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement3Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement3Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = announcement3NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = announcement3NudgeScreenShot.getSize().getWidth();
                int eleHeight = announcement3NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "announcement3NudgeImage", announcement3NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement3Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = announcement3NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = announcement3NudgeScreenShot.getSize().getWidth();
                int eleHeight = announcement3NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "announcement3NudgeImage", announcement3NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "announcement3NudgeImage", announcement3NudgeScreenShot)));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            e.printStackTrace();
        }

    }
    @Step("Announcement -3 nudge match with expected screenshot")
    public void nudgeMatch() throws InterruptedException, Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement3Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedAnnouncement3Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Get the adb log of Announcement3 nudge")
    public Announcement3NudgeObject getAnnouncement3Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.json");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] screenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}
