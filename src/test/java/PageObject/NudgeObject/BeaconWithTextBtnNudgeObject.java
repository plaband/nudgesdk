package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class BeaconWithTextBtnNudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement beaconWithTextBtnNudgeScreenShot;
    private WebElement listInList;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1Element;
    public BeaconWithTextBtnNudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Check Label1 Text of Beacon With Text Btn Nudge")
    public BeaconWithTextBtnNudgeObject getLabel1Text(){
        if (label1Element.getText().equals("Got a question, Search for the answer. Chances are someone has your back.")){
            test.log(LogStatus.PASS, "Label1 Text is matching");
        }else {
            test.log(LogStatus.FAIL, "Label1 Text is not matching");
        }
        return this;
    }
    @Step("Check btn1 Text of Beacon With Text Btn Nudge")
    public BeaconWithTextBtnNudgeObject getBtn1Text(){
        if (btn1Element.getText().equals("GOT IT")){
            test.log(LogStatus.PASS, "Btn1 Text is matching");
        }else {
            test.log(LogStatus.FAIL, "Btn1 Text is not matching");
        }
        return this;
    }
    @Step("Click on Btn1")
    public BeaconWithTextBtnNudgeObject clickBtn1() throws InterruptedException {
        if (btn1Element.isDisplayed()&&btn1Element.isDisplayed()){
            btn1Element.click();
            test.log(LogStatus.PASS,"Btn1 is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
        }
        else {
            test.log(LogStatus.FAIL,"Btn1 is not clicked");
        }
        return this;
    }
    @Step("Take ScreenShots of Beacon With Text Btn Nudge")
    public void BeaconWithTextBtnNudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualBeaconWithTextBtnNudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualTooltipWithTextBtn png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualBeaconWithTextBtnNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = beaconWithTextBtnNudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = beaconWithTextBtnNudgeScreenShot.getSize().getWidth();
                int eleHeight = beaconWithTextBtnNudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "beaconWithTextBtnNudgeImage", beaconWithTextBtnNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualBeaconWithTextBtnNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = beaconWithTextBtnNudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = beaconWithTextBtnNudgeScreenShot.getSize().getWidth();
                int eleHeight = beaconWithTextBtnNudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "beaconWithTextBtnNudgeImage", beaconWithTextBtnNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "beaconWithTextBtnNudgeImage", beaconWithTextBtnNudgeScreenShot)));
            e.printStackTrace();
        }
    }
    @Step("Compare Captured Beacon With Text Btn screenshots with expected Screenshots")
    public void nudgeMatch() throws InterruptedException, Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualBeaconWithTextBtnNudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedBeaconWithTextBtnNudge.png", file);
        //captureScreenShots();
    }
    @Step("Move app to background for 10 seconds")
    public BeaconWithTextBtnNudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Beacon With Text Btn nudge")
    public BeaconWithTextBtnNudgeObject getBeaconWithTextBtnLog() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
}
