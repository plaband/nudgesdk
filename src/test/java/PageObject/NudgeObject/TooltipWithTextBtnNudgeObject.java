package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.xml.bind.v2.model.core.ID;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class TooltipWithTextBtnNudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement tooltipWithTextBtnNudgeScreenShot;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='START MILTONGREEN']")
    private WebElement miltonGreenBtn;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='POINT AND CLICK']")
    private WebElement pointClickBtn;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FIRE EVENT']")
    private WebElement fireEventBtn;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='All Repeatables']")
    private WebElement allRepeatable;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='List in list']")
    private WebElement listInList;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1Element;
    public TooltipWithTextBtnNudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Click MiltonGreen Button")
    public TooltipWithTextBtnNudgeObject miltonGreenBtn_click() throws InterruptedException, IOException {
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("target/screenShots/welcomeObject.png"));
        try {
            if (miltonGreenBtn.isDisplayed() && miltonGreenBtn.isEnabled()) {
                miltonGreenBtn.click();
                test.log(LogStatus.PASS, "miltonGreenBtn is clicked");
                test.log(LogStatus.PASS, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
                Thread.sleep(1000);

            } else {
                test.log(LogStatus.FAIL, "miltonGreenBtn is not clicked");
                test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));

            }
        }catch (Exception e){
            test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
        }
        return this;

    }
    @Step("Click on point & Click Btn")
    public TooltipWithTextBtnNudgeObject pointClickBtn_click() throws InterruptedException {
        if (pointClickBtn.isDisplayed()&&pointClickBtn.isDisplayed()){
            pointClickBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
        }
        return this;
    }
    @Step("Click on the Fire Event button")
    public TooltipWithTextBtnNudgeObject fireEventBtn_click() throws InterruptedException {
        if (fireEventBtn.isDisplayed()&&fireEventBtn.isDisplayed()){
            fireEventBtn.click();
            test.log(LogStatus.PASS,"Fire Event is clicked");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"Fire Event is not clicked");
        }
        return this;
    }
    @Step("Click on All Repeatables")
    public TooltipWithTextBtnNudgeObject clickAllRepeatable() throws InterruptedException {
        if (allRepeatable.isDisplayed()&&allRepeatable.isDisplayed()){
            allRepeatable.click();
            test.log(LogStatus.PASS,"All Repeatables is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"All Repeatables is not clicked");
        }
        return this;
    }
    @Step("Click on List in List view")
    public TooltipWithTextBtnNudgeObject clickListInList() throws InterruptedException {
        if (listInList.isDisplayed()&&listInList.isDisplayed()){
            listInList.click();
            test.log(LogStatus.PASS,"List in List is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"List in List is not clicked");
        }
        return this;
    }
    @Step("Check Label1 Text of Tooltip With Text Btn Nudge")
    public TooltipWithTextBtnNudgeObject getLabel1Text(){
        if (label1Element.getText().equals("Got a question, Search for the answer. Chances are someone has your back.")){
            test.log(LogStatus.PASS, "Label1 Text is matching");
        }else {
            test.log(LogStatus.FAIL, "Label1 Text is not matching");
        }
        return this;
    }
    @Step("Check btn1 Text of Tooltip With Text Btn Nudge")
    public TooltipWithTextBtnNudgeObject getBtn1Text(){
        if (btn1Element.getText().equals("GOT IT")){
            test.log(LogStatus.PASS, "Btn1 Text is matching");
        }else {
            test.log(LogStatus.FAIL, "Btn1 Text is not matching");
        }
        return this;
    }
    @Step("Click on Btn1")
    public TooltipWithTextBtnNudgeObject clickBtn1() throws InterruptedException {
        if (btn1Element.isDisplayed()&&btn1Element.isDisplayed()){
            btn1Element.click();
            test.log(LogStatus.PASS,"Btn1 is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
        }
        else {
            test.log(LogStatus.FAIL,"Btn1 is not clicked");
        }
        return this;
    }
    @Step("Take ScreenShots of Tooltip With Text Btn Nudge")
    public void TooltipWithTextBtnNudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualTooltipWithTextBtnNudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualTooltipWithTextBtn png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualTooltipWithTextBtnNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = tooltipWithTextBtnNudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = tooltipWithTextBtnNudgeScreenShot.getSize().getWidth();
                int eleHeight = tooltipWithTextBtnNudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "tooltipWithTextBtnNudgeImage", tooltipWithTextBtnNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualTooltipWithTextBtnNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = tooltipWithTextBtnNudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = tooltipWithTextBtnNudgeScreenShot.getSize().getWidth();
                int eleHeight = tooltipWithTextBtnNudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "tooltipWithTextBtnNudgeImage", tooltipWithTextBtnNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "tooltipWithTextBtnNudgeImage", tooltipWithTextBtnNudgeScreenShot)));
        }
    }
    @Step("Compare Captured Tooltip With Text Btn screenshots with expected Screenshots")
    public void nudgeMatch() throws InterruptedException, Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualTooltipWithTextBtnNudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedTooltipWithTextBtnNudge.png", file);
        //captureScreenShots();
    }
    @Step("Move app to background for 10 seconds")
    public TooltipWithTextBtnNudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Tooltip With Text Btn nudge")
    public TooltipWithTextBtnNudgeObject getTooltipWithTextBtnLog() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
}
