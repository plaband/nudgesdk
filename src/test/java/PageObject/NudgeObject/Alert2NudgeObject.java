package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class Alert2NudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement alert2NudgeScreenShot;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1Element;

    public Alert2NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Get label1 Text of Alert- 2 nudge")
    public Alert2NudgeObject getLabel1Text(){
        if (label1Element.getText().equals("Looks like you are not connected to internet!")){
            test.log(LogStatus.PASS, "label1 text is matched");
        }else{
            test.log(LogStatus.FAIL, "label1 Text is not matched");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Get Btn1 Text of Alert 2 nudge")
    public Alert2NudgeObject getBtn1Text(){
        if (btn1Element.getText().equals("VIEW DOWNLOADS")){
            test.log(LogStatus.PASS,"Btn1 is visible and matched");
        }else {
            test.log(LogStatus.FAIL, "Btn1 is not clickable");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Btn1 is Clickable or not")
    public Alert2NudgeObject clickBtn1(){
        if (btn1Element.isDisplayed() && btn1Element.isEnabled()){
            btn1Element.click();
            test.log(LogStatus.PASS, "Btn1 is clicked");
        }else {
            test.log(LogStatus.FAIL,"Btn1 is not clickable");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    @Step("Take ScreenShots of Alert-2 Nudge")
    public void Alert2NudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert2Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert2Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = alert2NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = alert2NudgeScreenShot.getSize().getWidth();
                int eleHeight = alert2NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "alert2Image", alert2NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert2Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = alert2NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = alert2NudgeScreenShot.getSize().getWidth();
                int eleHeight = alert2NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "alert2Image", alert2NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "alert2Image", alert2NudgeScreenShot)));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            e.printStackTrace();
        }
    }
    @Step("Compare Captured Alert2 screenshots with expected Screenshots")
    public void nudgeMatch() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAlert2Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedAlert2Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Move app to background for 10 seconds")
    public Alert2NudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Alert-2 nudge")
    public Alert2NudgeObject getAlert2Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] screenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}
