package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class Promotion2NudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement promotion2NudgeScreenShot;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label_h")
    private WebElement labelHElement;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/img_h")
    private WebElement imgHElement;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn2")
    private WebElement btn2Element;

    public Promotion2NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Get label_h text from promotion4 Nudge")
    public Promotion2NudgeObject getLabelHText(){
        if (labelHElement.getText().equals("Item in your cart is on a flash sale. Get it now to save big!")){
            test.log(LogStatus.PASS, "label_h text is matching");
        }else {
            test.log(LogStatus.FAIL, "label_h text is not matching");
        }
        return this;
    }
    @Step("Get imgH Element from promotion4 Nudge")
    public Promotion2NudgeObject getImgHText(){
        if (imgHElement.isDisplayed()){
            test.log(LogStatus.PASS, "imgH Element is Present");
        }else {
            test.log(LogStatus.FAIL, "imgH Element is not Present");
        }
        return this;
    }
    @Step("Get Btn1 text from promotion-2 Nudge")
    public Promotion2NudgeObject getBtn1Text(){
        if (btn1Element.getText().equals("NOT NOW")){
            test.log(LogStatus.PASS, "Btn1 text is matching");
        }else {
            test.log(LogStatus.FAIL, "Btn1 text is not matching");
        }
        return this;
    }
    @Step("Get Btn2 text from promotion-2 Nudge")
    public Promotion2NudgeObject getBtn2Text(){
        if (btn2Element.getText().equals("VIEW ITEMS")){
            test.log(LogStatus.PASS, "Btn2 text is matching");
        }else {
            test.log(LogStatus.FAIL, "Btn2 text is not matching");
        }
        return this;
    }
    @Step("Check if Btn2 is clicked or not for promotion-2 Nudge")
    public Promotion2NudgeObject clickBtn2(){
        if (btn2Element.isDisplayed() && btn2Element.isEnabled()){
            btn2Element.click();
            test.log(LogStatus.PASS, "Btn2 is Clicked");
        }else {
            test.log(LogStatus.FAIL, "Btn2 is not Clicked");
        }
        return this;
    }
    @Step("Take ScreenShots of Promotion-2 Nudge")
    public void promotion2NudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion2Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion2Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = promotion2NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = promotion2NudgeScreenShot.getSize().getWidth();
                int eleHeight = promotion2NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "promotion2NudgeImage", promotion2NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion2Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = promotion2NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = promotion2NudgeScreenShot.getSize().getWidth();
                int eleHeight = promotion2NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "promotion2NudgeImage", promotion2NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "promotion2NudgeImage", promotion2NudgeScreenShot)));
            e.printStackTrace();
        }
    }
    @Step("Compare Captured Promotion-2 screenshots with expected Screenshots")
    public void nudgeMatch() throws InterruptedException, Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualPromotion2Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedPromotion2Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Move app to background for 10 seconds")
    public Promotion2NudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Promotion-2 nudge")
    public Promotion2NudgeObject getPromotion2Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }

}
