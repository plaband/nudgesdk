package PageObject.NudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.Duration;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.io.ByteArrayInputStream;

public class Action1NudgeObject extends ExtentReport {

    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement action1NudgeScreenShot;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label2")
    private WebElement label2Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn2")
    private WebElement btn2Element;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='START MILTONGREEN']")
    private WebElement miltonGreenBtn;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='POINT AND CLICK']")
    private WebElement pointClickBtn;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FIRE EVENT']")
    private WebElement fireEventBtn;


    public Action1NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Click MiltonGreen Butron")
    public Action1NudgeObject miltonGreenBtn_click() throws InterruptedException, IOException {
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("target/screenShots/welcomeObject.png"));
        try {
            if (miltonGreenBtn.isDisplayed() && miltonGreenBtn.isEnabled()) {
                miltonGreenBtn.click();
                test.log(LogStatus.PASS, "miltonGreenBtn is clicked");
                test.log(LogStatus.PASS, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
                Thread.sleep(1000);

            } else {
                test.log(LogStatus.FAIL, "miltonGreenBtn is not clicked");
                test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));

            }
        }catch (Exception e){
            test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;

    }
    @Step("Click on point & Click Btn")
    public Action1NudgeObject pointClickBtn_click() throws InterruptedException {
        if (pointClickBtn.isDisplayed()&&pointClickBtn.isDisplayed()){
            pointClickBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Click on the Fire Event button")
    public Action1NudgeObject fireEventBtn_click() throws InterruptedException {
        if (fireEventBtn.isDisplayed()&&fireEventBtn.isDisplayed()){
            fireEventBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Label1 Text Matching")
    @Description("Label1 text matching")
    public Action1NudgeObject getLabel1Text() throws Exception {
        if (label1Element.getText().equals("New Version Available")){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action1label1Element", label1Element)));
            test.log(LogStatus.PASS, "Label1 text is matching");
        }else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "action1label1Element", label1Element)));
            test.log(LogStatus.FAIL, "Label1 text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Label2 Text Matching")
    @Description("Label2 text matching")
    public Action1NudgeObject getLabel2Text() throws Exception {
        if (label2Element.getText().equals("Please update the app to the latest version to receive updated features")){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action1label2Element", label2Element)));
            test.log(LogStatus.PASS, "Label2 text is matching");
        }else {
            test.log(LogStatus.FAIL, "Label2 text is not matching");
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "action1label2Element", label2Element)));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button1 Text Matching")
    @Description("Button1 text matching")
    public Action1NudgeObject getButton1Text() throws Exception {
        if (btn1Element.getText().equals("NOT NOW")){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "miltonGreenBtn", btn1Element)));
            test.log(LogStatus.PASS, "Button1 text is matching");
        }else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "miltonGreenBtn", btn1Element)));
            test.log(LogStatus.FAIL, "Button1 text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button2 Text Matching")
    @Description("Button2 text matching")
    public Action1NudgeObject getButton2Text() throws Exception {
        if (btn2Element.getText().equals("UPDATE")){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action1Btn2Element", btn2Element)));
            test.log(LogStatus.PASS, "Button2 text is matching");
        }else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "action1Btn2Element", btn2Element)));
            test.log(LogStatus.FAIL, "Button2 text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button1 is clickable for not")
    @Description("Button1 is clickable for not")
    public Action1NudgeObject clickButton1() throws Exception {
        if (btn1Element.isDisplayed() && btn1Element.isEnabled()){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action1Btn1Element", btn1Element)));
            btn1Element.click();
            test.log(LogStatus.PASS, "Button1 is clicked");
        }else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "action1Btn1Element", btn1Element)));
            test.log(LogStatus.FAIL, "Button1 is not clickable");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button2 is clickable for not")
    @Description("Button2 is clickable for not")
    public Action1NudgeObject clickButton2() throws Exception {
        if (btn2Element.isDisplayed() && btn2Element.isEnabled()){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action1Btn2Element", btn2Element)));
            btn2Element.click();
            test.log(LogStatus.PASS, "Button1 is clicked");
        }else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "action1Btn2Element", btn2Element)));
            test.log(LogStatus.FAIL, "Button1 is not clickable");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }

    @Step("Take SceenShots of Action1 Nudge")
    public void Action1NudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction1Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction1Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = action1NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = action1NudgeScreenShot.getSize().getWidth();
                int eleHeight = action1NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action1Image", action1NudgeScreenShot)));
                Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction1Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = action1NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = action1NudgeScreenShot.getSize().getWidth();
                int eleHeight = action1NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                System.out.println(screenshotLocation.toString());
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action1Image", action1NudgeScreenShot)));
                Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            }

        } catch (IOException e) {
            e.printStackTrace();
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "action1Image", action1NudgeScreenShot)));
        }
    }
    @Step("Compare Captured Action1 screenshots with expected Screenshots")
    public void nudgeMatch() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction1Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedAction1Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Move app to background for 10 seconds")
    public Action1NudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Action-1 nudge")
    public Action1NudgeObject getAction1Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.json");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] screenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

}
