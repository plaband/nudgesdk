package PageObject.NudgeObject;

import PageObject.WelcomeNudgeObject;
import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class NpsNudgeObject extends ExtentReport {
    @AndroidFindBy(xpath = "//android.widget.Button[@text='START MILTONGREEN']")
    private WebElement miltonGreenBtn;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='POINT AND CLICK']")
    private WebElement pointClickBtn;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FIRE EVENT']")
    private WebElement fireEventBtn;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private  WebElement label1Text;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label3")
    private WebElement label3Text;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/layout_nps")
    private WebElement npsLayout;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Least Likely']")
    private WebElement leastLikelyText;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Very Likely']")
    private WebElement veryLikelyText;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement npsNudgeScreenShot;
    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='5']")
    private WebElement rating;

    public NpsNudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Click MiltonGreen Butron")
    public NpsNudgeObject miltonGreenBtn_click() throws InterruptedException, IOException {
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("target/screenShots/welcomeObject.png"));
        try {
            if (miltonGreenBtn.isDisplayed() && miltonGreenBtn.isEnabled()) {
                miltonGreenBtn.click();
                test.log(LogStatus.PASS, "miltonGreenBtn is clicked");
                test.log(LogStatus.PASS, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
                Thread.sleep(1000);

            } else {
                test.log(LogStatus.FAIL, "miltonGreenBtn is not clicked");
                test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));

            }
        }catch (Exception e){
            test.log(LogStatus.FAIL, test.addScreenCapture("src/test/screenShots/objectScreenShots/welcomeObject.png"));
        }
        return this;

    }
    @Step("Click on point & Click Btn")
    public NpsNudgeObject pointClickBtn_click() throws InterruptedException {
        if (pointClickBtn.isDisplayed()&&pointClickBtn.isDisplayed()){
            pointClickBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            //test.addScreenCapture("target/screenShots/sreenShots1.png");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
        }
        return this;
    }
    @Step("Click on the Fire Event button")
    public NpsNudgeObject fireEventBtn_click() throws InterruptedException {
        if (fireEventBtn.isDisplayed()&&fireEventBtn.isDisplayed()){
            fireEventBtn.click();
            test.log(LogStatus.PASS,"pointClickBtn is clicked");
            Thread.sleep(1000);
        }
        else {
            test.log(LogStatus.FAIL,"pointClickBtn is not clicked");
        }
        return this;
    }
    @Step("Take on NpsNudge ScreenShot")
    public void NpsNudgeScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualNpsNudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualNpsNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = npsNudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = npsNudgeScreenShot.getSize().getWidth();
                int eleHeight = npsNudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "npsNudgeImage", npsNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualNpsNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = npsNudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = npsNudgeScreenShot.getSize().getWidth();
                int eleHeight = npsNudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "npsNudgeImage", npsNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "npsNudgeImage", npsNudgeScreenShot)));
            e.printStackTrace();
        }
    }
    @Step("Compare Nps screenshot with expected screenshot")
    public void nudgeMatch() throws InterruptedException, Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualNpsNudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedNpsNudge.png", file);
        //captureScreenShots();
    }
    @Step("get the Nps nudge label1 text")
    public NpsNudgeObject getLabel1Text(){
        if (label1Text.getText().equals("How would you rate us?")){
            test.log(LogStatus.PASS, "label1 text in NPS nudge is present");
        }else {
            test.log(LogStatus.FAIL, "label1 text in NPS nudge is not matching");
        }
        return this;
    }
    @Step("get the Nps nudge label3 text")
    public NpsNudgeObject getLabel3Text(){
        if (label3Text.getText().equals("On a scale from 0 to 10 how likely are you to...?")){
            test.log(LogStatus.PASS, "label3 text in NPS nudge is present");
        }else{
            test.log(LogStatus.FAIL, "label3 text in NPS nudge is not matching");
        }
        return this;
    }
    @Step("get Nps layout of NPS nudge")
    public NpsNudgeObject getNpsLayout(){
        if (npsLayout.isDisplayed()){
            test.log(LogStatus.PASS, "nps rating layout is visible");
        }else {
            test.log(LogStatus.FAIL, "nps rating layout is not visible");
        }
        return this;
    }
    @Step("get getLeastLikelyText of Nps NUDGE")
    public NpsNudgeObject getLeastLikelyText(){
        if (leastLikelyText.getText().equals("Least Likely")){
            test.log(LogStatus.PASS, "LeastLikelyText is matching");
        }else{
            test.log(LogStatus.FAIL, "LeastLikelyText is not matching");
        }
        return this;
    }
    @Step("get getVeryLikelyText of Nps NUDGE")
    public NpsNudgeObject getVeryLikelyText(){
        if (veryLikelyText.getText().equals("Very Likely")){
            test.log(LogStatus.PASS, "VeryLikelyText is matching");
        }else {
            test.log(LogStatus.FAIL, "VeryLikelyText is not matching");
        }
        return this;
    }
    @Step("get btn1 text of Nps NUDGE")
    public NpsNudgeObject getBtn1Text(){
        if (btn1.getText().equals("SUBMIT")){
            test.log(LogStatus.PASS, "Button text is SUBMIT");
        }else {
            test.log(LogStatus.FAIL, "Button text is not SUBMIT");
        }
        return this;
    }
    @Step("Click the btn1 of Nps NUDGE")
    public NpsNudgeObject btn1Click(){
        if (btn1.isDisplayed() && btn1.isEnabled()){
            btn1.click();
            test.log(LogStatus.PASS, "Submit button is clicked");
        }else {
            test.log(LogStatus.FAIL, "Submit button is not clicked");
        }
        return this;
    }
    @Step("Select NPS rating of Nps NUDGE")
    public NpsNudgeObject selectRating(){
        if (rating.isDisplayed() && rating.isEnabled()){
            rating.click();
            test.log(LogStatus.PASS, "rating 5 is selected");
        }else {
            test.log(LogStatus.FAIL, "rating 5 is not selected");
        }
        return this;
    }
    @Step("Move app to background for 10 seconds")
    public NpsNudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of Net Promoter Score nudge")
    public NpsNudgeObject getNpsNudgeLog() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }

}
