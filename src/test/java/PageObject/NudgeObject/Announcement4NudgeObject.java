package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Announcement4NudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement announcement4NudgeScreenShot;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Text;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/img1")
    private WebElement announcement4Img;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn2")
    private WebElement btn2;

    public Announcement4NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Attachment(value = "Screenshot", type = "image/png")
    public  byte[] takeScreenshot() {
        // Take a screenshot as byte array and return
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }
    @Step("Image Container Test")
    @Description("Image Container Test")
    public Announcement4NudgeObject getAnnouncement2Img(){
        if (announcement4Img.isDisplayed()){
            test.log(LogStatus.PASS, "Image Container is Present");
        }else {
            test.log(LogStatus.FAIL, "image container mismatch");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Label1 Text Checking")
    @Description("Label1 Text Checking")
    public Announcement4NudgeObject getLabel1Text(){
        if (label1Text.getText().equals("You can now search for your friend's location...")){
            test.log(LogStatus.PASS, "Label1 Text is matching");
        }else {
            test.log(LogStatus.FAIL, "Label1 Text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button1 Text Checking")
    @Description("button1 Text checking")
    public Announcement4NudgeObject getBtn1Text(){
        if (btn1.getText().equals("OK, GOT IT")){
            test.log(LogStatus.PASS, "button1 Text is matching");
        }else {
            test.log(LogStatus.FAIL, "button1 Text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("button2 Text checking")
    @Description("button2 Text checking")
    public Announcement4NudgeObject getBtn2Text(){
        if (btn2.getText().equals("EXPLORE")){
            test.log(LogStatus.PASS, "button2 Text is matching");
        }else {
            test.log(LogStatus.FAIL, "button2 Text is not matching");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Check Button1 click for Announcement-4 Nudge")
    @Description("Check Button1 click for Announcement-4 Nudge")
    public Announcement4NudgeObject clickBtn1(){
        if (btn1.isDisplayed() && btn1.isEnabled()){
            btn1.click();
            test.log(LogStatus.PASS, "button1 is Clicked");
        }else {
            test.log(LogStatus.FAIL,"Button1 is not clickable");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Check Button2 click for Announcement-4 Nudge")
    @Description("Check Button2 click for Announcement-4 Nudge")
    public Announcement4NudgeObject clickBtn2(){
        if (btn2.isDisplayed() && btn2.isEnabled()){
            btn2.click();
            test.log(LogStatus.PASS, "Button2 is Clicked");
        }else {
            test.log(LogStatus.FAIL,"Button2 is not clickable");
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Take announcement4 Nudge ScreenShot")
    public void announcement4NudgeScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement4Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement4Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = announcement4NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = announcement4NudgeScreenShot.getSize().getWidth();
                int eleHeight = announcement4NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "announcement4NudgeImage", announcement4NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement4Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = announcement4NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = announcement4NudgeScreenShot.getSize().getWidth();
                int eleHeight = announcement4NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "announcement4NudgeImage", announcement4NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "announcement4NudgeImage", announcement4NudgeScreenShot)));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }

    }
    @Step("Match announcement4 screenshot with expected screenshot")
    public void nudgeMatch() throws InterruptedException, Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAnnouncement4Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedAnnouncement4Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Get the adb log of Announcement4 nudge")
    public Announcement4NudgeObject getAnnouncement4Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.json");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
}
