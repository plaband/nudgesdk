package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.xml.bind.v2.model.core.ID;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;
import java.io.ByteArrayInputStream;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Action2NudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement action2NudgeScreenShot;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private WebElement label1Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label2")
    private WebElement label2Element;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement btn1Element;

    public Action2NudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Label1 Text Matching")
    @Description("Label1 text matching")
    public Action2NudgeObject getLabel1Text() throws Exception {
        if (label1Element.getText().equals("New Version Available")){
            test.log(LogStatus.PASS, "Label1 text is matching");
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "label1Image", label1Element)));
        }else {
            test.log(LogStatus.FAIL, "Label1 text is not matching");
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "label1Image", label1Element)));

        }
        return this;
    }
    @Step("Label2 Text Matching")
    @Description("Label2 text matching")
    public Action2NudgeObject getLabel2Text() throws Exception {
        if (label2Element.getText().equals("Please update the app to the latest version to receive updated features")){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "label2Image", label2Element)));
            test.log(LogStatus.PASS, "Label2 text is matching");
        }else {
            test.log(LogStatus.FAIL, "Label2 text is not matching");
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "label2Image", label2Element)));
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
        }
        return this;
    }
    @Step("Button1 Text Matching")
    @Description("Button1 text matching")
    public Action2NudgeObject getButton1Text() throws Exception {
        if (btn1Element.getText().equals("UPDATE")){
            test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "btn12Image", btn1Element)));
            test.log(LogStatus.PASS, "Button1 text is matching");
        }else {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "btn12Image", btn1Element)));
            test.log(LogStatus.FAIL, "Button1 text is not matching");
        }
        return this;
    }
    @Step("Button1 is clickable for not")
    @Description("Button1 is clickable for not")
    public Action2NudgeObject clickButton1(){
        if (btn1Element.isDisplayed() && btn1Element.isEnabled()){
            btn1Element.click();
            test.log(LogStatus.PASS, "Button1 is clicked");
        }else {
            test.log(LogStatus.FAIL, "Button1 is not clickable");
        }
        return this;
    }

    @Step("Take SceenShots of Action2 Nudge")
    public void Action2NudgeObjectScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction2Nudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction2Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = action2NudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = action2NudgeScreenShot.getSize().getWidth();
                int eleHeight = action2NudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action2Image", action2NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());

            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction2Nudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = action2NudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = action2NudgeScreenShot.getSize().getWidth();
                int eleHeight = action2NudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "action2Image", action2NudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "action2Image", action2NudgeScreenShot)));
            e.printStackTrace();
        }
    }
    @Step("Compare Captured Action2 screenshots with expected Screenshots")
    public void nudgeMatch() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualAction2Nudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedAction2Nudge.png", file);
        //captureScreenShots();
    }
    @Step("Get the adb log for Action -2 nudge")
    public Action2NudgeObject getAction2Log() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.json");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }
    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] screenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

}
