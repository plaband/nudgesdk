package PageObject.NudgeObject;

import Utility.CompareImages;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.PageFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class UserFeedbackNudgeObject extends ExtentReport {
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label1")
    private  WebElement label1Text;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/label2")
    private WebElement label2Text;
    @AndroidFindBy(id = "android:id/text1")
    private WebElement textView;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/input1")
    private WebElement enterTextView;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn1")
    private WebElement cancelBtn;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/btn2")
    private WebElement okBtn;
    @AndroidFindBy(id = "io.hansel.sampleapp.userjourney.miltongreen:id/prompt")
    private WebElement feedbackNudgeScreenShot;

    public UserFeedbackNudgeObject(AndroidDriver driver) {
        ExtentReport.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @Step("Take SceenShots of UserFeedbackNudge")
    public void UserFeedbackNudgeScreenShot() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        System.out.println(deviceName);
        File screenshotLocation = null;
        String file1 = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualFeedBackNudge.png";
        File temp = new File(file1);
        boolean exists = temp.exists();
        try{
            if (exists == true) {
                temp.delete();
                test.log(LogStatus.INFO, "actualWecomeNudge png exists hence deleted");
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualFeedBackNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
                //Get the location of element on the page
                Point point = feedbackNudgeScreenShot.getLocation();
                //Get width and height of the element
                int eleWidth = feedbackNudgeScreenShot.getSize().getWidth();
                int eleHeight = feedbackNudgeScreenShot.getSize().getHeight();
                //Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(file);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "feedBackImage", feedbackNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }else{
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String elseFile = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualFeedBackNudge.png";
                BufferedImage fullImg = ImageIO.read(scrFile);
//Get the location of element on the page
                Point point = feedbackNudgeScreenShot.getLocation();
//Get width and height of the element
                int eleWidth = feedbackNudgeScreenShot.getSize().getWidth();
                int eleHeight = feedbackNudgeScreenShot.getSize().getHeight();
//Crop the entire page screenshot to get only element screenshot
                BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth,
                        eleHeight);
                ImageIO.write(eleScreenshot, "png", scrFile);
                screenshotLocation = new File(elseFile);
                FileUtils.copyFile(scrFile, screenshotLocation);
                test.log(LogStatus.PASS,test.addScreenCapture(getScreenShot(driver, "feedBackImage", feedbackNudgeScreenShot)));
                System.out.println(screenshotLocation.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
            test.log(LogStatus.FAIL,test.addScreenCapture(getScreenShot(driver, "feedBackImage", feedbackNudgeScreenShot)));
        }
    }
    @Step("Compare Captured User Feedback screenshots with expected Screenshots")
    public void nudgeMatch() throws Exception {
        Map<String, Object> caps = driver.getSessionDetails();
        String deviceManufacturer = (String) caps.get("deviceManufacturer");
        String deviceModel = (String) caps.get("deviceModel");
        //String deviceFullName = (String) caps.get("DeviceFullName");
        String deviceName = deviceManufacturer+ "_" +deviceModel;
        String file = "src/test/screenShots/actualWelcomeNudge" + "/" + deviceName + "_" + "actualFeedBackNudge.png";
        System.out.println(file);
        CompareImages cmpi = new CompareImages();
        CompareImages.compareImages("src/test/screenShots/expectedWelcomeNudge/expectedFeedBackNudge.png", file);
        //captureScreenShots();
    }
    @Step("Get the label1 Text of User Feedback nudge")
    public UserFeedbackNudgeObject getLabel1Text(){
        if (label1Text.getText().equals("Your opinion matters")){
            test.log(LogStatus.PASS, "label1 text in NPS nudge is present");
        }else {
            test.log(LogStatus.FAIL, "label1 text in NPS nudge is not matching");
        }
        return this;
    }
    @Step("Get the label2 Text of User Feedback nudge")
    public UserFeedbackNudgeObject getLabel2Text(){
        if (label2Text.getText().equals("Your feedback helps us improve our service and support. Please take a minute to let us help you provide the best possible experience.")){
            test.log(LogStatus.PASS, "label2 text in NPS nudge is present");
        }else{
            test.log(LogStatus.FAIL, "label2 text in NPS nudge is not matching");
        }
        return this;
    }
    @Step("Get the TextView of User Feedback nudge")
    public UserFeedbackNudgeObject getTextView(){
        if (textView.isDisplayed()){
            test.log(LogStatus.PASS, "TextView  is visible");
        }else {
            test.log(LogStatus.FAIL, "TextView is not visible");
        }
        return this;
    }
    @Step("Enter FeedBack in the User Feedback nudge")
    public UserFeedbackNudgeObject enterTextView(String text){
        if (enterTextView.isDisplayed()) {
            enterTextView.clear();
            enterTextView.sendKeys(text);
            test.log(LogStatus.PASS, "text is entered :",text);
        }else {
            test.log(LogStatus.PASS, "text is not entered ");
        }
        return this;
    }
    @Step("Get the Cancel Button Text of User Feedback nudge")
    public UserFeedbackNudgeObject getCancelBtn(){
        if (cancelBtn.getText().equals("CANCEL")){
            test.log(LogStatus.PASS, "Button text is CANCEL");
        }else {
            test.log(LogStatus.FAIL, "Button text is not CANCEL");
        }
        return this;
    }
    @Step("Get the OK Button Text of User Feedback nudge")
    public UserFeedbackNudgeObject getOkBtn(){
        if (okBtn.getText().equals("OK")){
            test.log(LogStatus.PASS, "Button text is OK");
        }else {
            test.log(LogStatus.FAIL, "Button text is not OK");
        }
        return this;
    }
    @Step("Click the OK Button Text of User Feedback nudge")
    public UserFeedbackNudgeObject okBtnClick() {
        if (okBtn.isDisplayed() && okBtn.isEnabled()) {
            okBtn.click();
            test.log(LogStatus.PASS, "OK button is clicked");
        } else {
            test.log(LogStatus.FAIL, "OK button is not clicked");
        }
        return this;
    }
    @Step("Move the app to background for 10 milliseconds")
    public UserFeedbackNudgeObject app_backGround(){
        driver.runAppInBackground(Duration.ofSeconds(10));
        return this;
    }
    @Step("Get the adb log of User Feedback nudge")
    public UserFeedbackNudgeObject getFeedBackLog() throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                String s = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(s);
                writer.close();
            }

        }
        return this;
    }

}
