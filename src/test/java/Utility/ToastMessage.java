package Utility;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept.PIX;
import org.testng.Assert;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;
import static org.bytedeco.javacpp.tesseract.TessBaseAPI;

public class ToastMessage {

    public String getToastMessage() {

        String filePath = System.getProperty("target/toastmessages");

        String str = "";
        BytePointer outText;
        TessBaseAPI api = new TessBaseAPI();
        if (api.Init("target/tessdata", "ENG") != 0) {
            System.err.println("Could not initialize tesseract.");
            System.exit(1);
        }

        PIX image = pixRead("target/toastmessages/Configssync.png");
        api.SetImage(image);

        // Get OCR result
        outText = api.GetUTF8Text();
        str = outText.getString();
        Assert.assertTrue(!str.isEmpty());
        System.out.println("OCR output:\n" + str);

        // Destroy used object and release memory
        api.End();
        outText.deallocate();
        pixDestroy(image);
        return str;


    }

}
