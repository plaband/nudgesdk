package Utility;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Set_Up {
    Environment testEnvironment;
    public static URL url;
    public static DesiredCapabilities capabilities;
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;
    public static AndroidDriver driver;
    protected WebDriverWait wait;
    @BeforeSuite
    public void setupAppium() throws MalformedURLException {
        //ConfigFactory.setProperty("env", environment);
        //testEnvironment = ConfigFactory.create(Environment.class);
        String apkpath="./miltongreen-nonproguarded-userjourney.apk";
        File app=new File(apkpath);
        //setting up desired capability
        DesiredCapabilities caps = new DesiredCapabilities();
        //caps.setCapability("browserName", "emulator");
        caps.setCapability("platform", "ANDROID");
        caps.setCapability("platformName","Android");
        caps.setCapability("platformVersion", "7.1.2");
        caps.setCapability("deviceName", "Redmi");
        caps.setCapability("app", app.getAbsolutePath());
        //caps.setCapability("--session-override",true);
        caps.setCapability("noReset","true");
        caps.setCapability("fullReset","false");
        caps.setCapability("appPackage", "io.hansel.sampleapp.userjourney.miltongreen");
        //caps.setCapability("appActivity","io.visualizerapp.app.MainActivity");
        caps.setCapability("appWaitActivity","com.company.miltongreen.activity.LaunchActivity");
        //caps.setCapability("appWaitActivity","io.hansel.hanselsampleapp.miltongreen/io.visualizerapp.LaunchActivity");
        //caps.setCapability("appWaitActivity","io.hansel.hanselsampleapp.miltongreen/io.somecompany.hanselsampleapp.HotelActivity");
        //caps.setCapability("appWaitActivity","com.health360.userapp.health360.ui.onboarding.OnboardingActivity");

        //initializing driver object
        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);

        //initializing explicit wait object
        wait = new WebDriverWait(driver, 10);

    }

    /*
    @AfterSuite
    public void uninstallApp() throws InterruptedException {
        driver.removeApp("io.hansel.PebbletraceDevelopment");
    }

    //6
    @Test(enabled = true)
    public void myFirstTest() throws InterruptedException {
        driver.resetApp();
    }*/

    @AfterTest
    public void afterTest(){
        driver.quit();

    }

}