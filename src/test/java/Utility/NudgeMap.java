package Utility;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;

public class NudgeMap {
    public  String text;
    public  Long fontSize;
    public  String fontFamily;
    public  String fontStyle;

    public NudgeMap(String text, Long fontSize, String fontFamily, String fontStyle){
        this.text = text;
        this.fontFamily= fontFamily;
        this.fontSize= fontSize;
        this.fontStyle= fontStyle;

    }

    public static NudgeMap label1Key(String nudgeNameScale) throws IOException, ParseException {
        BufferedReader reader = new BufferedReader(new FileReader("src/test/jsonFile/log.text"));
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        String ls = System.getProperty("line.separator");
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
// delete the last new line separator
        String s1 = stringBuilder.substring(stringBuilder.indexOf("{"));

        reader.close();

        String content = stringBuilder.toString();
        String content1 = content.trim();
        System.out.println(content1);
        String s2 = content.substring(content1.indexOf("{"));
        s2.trim();
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject)jsonParser.parse(s2);
        System.out.println(jsonObject);
        JSONObject label1Object = (JSONObject) jsonObject.get("label1");
        String text = (String) label1Object.get("text");
        Long fontSize = (Long) label1Object.get("fontSize");
        String fontFamily = (String) label1Object.get("fontFamily");
        String fontStyle = (String) label1Object.get("fontStyle");

        return new NudgeMap(text, fontSize, fontFamily, fontStyle);

    }
    public static NudgeMap label21Key(String nudgeNameScale) throws IOException, ParseException {
        BufferedReader reader = new BufferedReader(new FileReader("src/test/jsonFile/log.text"));
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        String ls = System.getProperty("line.separator");
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
// delete the last new line separator
        String s1 = stringBuilder.substring(stringBuilder.indexOf("{"));

        reader.close();

        String content = stringBuilder.toString();
        String content1 = content.trim();
        System.out.println(content1);
        String s2 = content.substring(content1.indexOf("{"));
        s2.trim();
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject)jsonParser.parse(s2);
        System.out.println(jsonObject);
        JSONObject label1Object = (JSONObject) jsonObject.get("label2");
        String text = (String) label1Object.get("text");
        Long fontSize = (Long) label1Object.get("fontSize");
        String fontFamily = (String) label1Object.get("fontFamily");
        String fontStyle = (String) label1Object.get("fontStyle");

        return new NudgeMap(text, fontSize, fontFamily, fontStyle);

    }
    public static void main(String[] args) throws Exception {
//        GetNudgeParser getNudgeParser = GetNudgeParser.getImage1Key("url", "src/test/jsonFile/announcementNudgeJourney.json", "Announcement - 4");
//        String url = getNudgeParser.url;
//        System.out.println(url);
//        NudgeMap nudgeMap = label1Key("Announcement - 1");
//        System.out.println(nudgeMap);
//        Long fontSize = label1Key("Announcement - 1").fontSize;
//        System.out.println(fontSize);
//        Long fontSize1 = label21Key("Announcement - 1").fontSize;
//        System.out.println(fontSize);

    }

}