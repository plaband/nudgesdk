package Utility;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class PropertyMatcher {
    public void btn2fontSizeMatch(String nudgeMapKey, String nudgeParserKey, String filePath, String nudgeName) throws Exception {
        String nudge_Property = "{\"width\": 10, \"height\": 20, \"button1_size\": 5,\"button2_size\": 9,\"nudge_radius\": 6,\"nudge_pos_x\": 12,\"nudge_pos_y\": 18}\n";
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Integer> nudge_properties_map = objectMapper.readValue(nudge_Property, Map.class);
        Integer btn2Size = nudge_properties_map.get(nudgeMapKey);
        GetNudgeParser btn2getNudgeParser = GetNudgeParser.getButton2Key(nudgeParserKey, filePath, nudgeName);
        Long fontSize = btn2getNudgeParser.fontSize;
        int jBtn2Size = fontSize.intValue();
        if (btn2Size == jBtn2Size){
            System.out.println("Value matched");
        }else{
            System.out.println("mismatched");
        }
    }


    public static void main(String[] args) throws Exception {
        PropertyMatcher propertyMatcher = new PropertyMatcher();
        propertyMatcher.btn2fontSizeMatch("button2_size", "fontSize", "src/test/jsonFile/announcementNudgeJourney.json", "Announcement - 4");
//        String prod = "prod";
//        String fi = "src/main/resources/" + prod + ".properties";
//        File file = new File(fi);
//        System.out.println(file);
//        ConfigFileReader configFileReader = new ConfigFileReader();
//        System.out.println(configFileReader.getApplicationUrl());
    }
}
