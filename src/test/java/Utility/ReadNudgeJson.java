package Utility;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class ReadNudgeJson{

    public String readNudge(int i, String jsonKey, Object nudgeName, Object objectName) {
        String jKey = null;
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("src/test/jsonFile/nudgeObject.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            //JSONObject nudgeObject = new JSONObject();
            JSONArray nudgeList = (JSONArray) obj;
            //System.out.println(employeeList);
            JSONObject nudgeObject = (JSONObject) nudgeList.get(i);
            //System.out.println(nudgeObject);
            JSONObject promptName = (JSONObject) nudgeObject.get(objectName);
            jKey = (String) promptName.get(jsonKey);
            //System.out.println(firstName);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return jKey;
    }

}
