package Utility;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class GetNudgeParser {
    public Long fontSize;
    public  Long cornerRadius;
    public  String textColor;
    public  String text;
    public  String imgName;
    public String height;
    public  String fontStyle;
    public  String fontFamily;
    public  String borderColor;
    public  String bgColor;
    public  String textVariables;
    public String spacing;
    public String width;
    public Boolean showDismissErrorMessage;
    public Boolean showBtnXOrDismissConditionError;
    public String position;
    public String maxWidth;
    public String maxHeight;
    public Long margin;
    public String btnGap;
    public String btnContainerSpacing;
    public Long bgOpacity;
    public Double backDropOpacity;
    public Boolean backdropDismiss;
    public String backDropColor;
    public Boolean allowAppInteraction;
    public  String actionType;
    public String type;
    public Double imgAspectRatio;
    public Long maxTopSpacing;
    public Long opacity;
    public String trackKey;
    public String url;

    public GetNudgeParser(Long fontSize, Long cornerRadius, String textColor, String text, String imgName, String height, String fontStyle, String fontFamily, String borderColor, String bgColor) {
        this.textColor = textColor;
        this.text = text;
        this.bgColor = bgColor;
        this.borderColor = borderColor;
        this.cornerRadius = cornerRadius;
        this.fontFamily = fontFamily;
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.height = height;
        this.imgName = imgName;
    }
    public GetNudgeParser(String textVariables, String spacing, String textColor, Long labelFontSize, String text, String fontStyle, String fontFamily) {
        this.textVariables = textVariables;
        this.spacing = spacing;
        this.fontSize = fontSize;
        this.textColor = textColor;
        this.text = text;
        this.fontStyle = fontStyle;
        this.fontFamily = fontFamily;
    }

    public GetNudgeParser(String width, String spacing, Boolean showDismissErrorMessage, Boolean showBtnXOrDismissConditionError, String position, String maxWidth, String maxHeight, Long margin, Long cornerRadius, String btnGap, String btnContainerSpacing, String borderColor, Long bgOpacity, String bgColor, Double backDropOpacity, Boolean backdropDismiss, String backDropColor, Boolean allowAppInteraction) {
        this.width = width;
        this.showDismissErrorMessage = showDismissErrorMessage;
        this.showBtnXOrDismissConditionError = showBtnXOrDismissConditionError;
        this.position = position;
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
        this.margin = margin;
        this.cornerRadius = cornerRadius;
        this.btnGap = btnGap;
        this.btnContainerSpacing = btnContainerSpacing;
        this.borderColor = borderColor;
        this.bgOpacity = bgOpacity;
        this.bgColor = bgColor;
        this.backDropOpacity = backDropOpacity;
        this.backdropDismiss = backdropDismiss;
        this.backDropColor = backDropColor;
        this.allowAppInteraction = allowAppInteraction;
    }

    public GetNudgeParser(Long fontSize, String actionType, String type, Long cornerRadius, String textColor, String text, String imgName, String height, String fontStyle, String fontFamily, String borderColor, String bgColor, String textVariables) {
        this.textColor = textColor;
        this.text = text;
        this.bgColor = bgColor;
        this.borderColor = borderColor;
        this.cornerRadius = cornerRadius;
        this.fontFamily = fontFamily;
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.height = height;
        this.imgName = imgName;
        this.textVariables = textVariables;
        this.actionType = actionType;
        this.type = type;
    }

    public GetNudgeParser(String height, Double imgAspectRatio, Long maxTopSpacing, String spacing, Long opacity, String trackKey, String url, String width) {
        this.height = height;
        this.imgAspectRatio = imgAspectRatio;
        this.maxTopSpacing = maxTopSpacing;
        this.spacing = spacing;
        this.opacity = opacity;
        this.trackKey = trackKey;
        this.url = url;
        this.width = width;

    }


    public static String readFileAsString(String file1) throws Exception {
        return new String(Files.readAllBytes(Paths.get(file1)));
    }

    public static Object getNudgeParser(String filePath, String nudgeName) throws Exception {
        Object changes = null;
        String file1 = filePath;
        String json = readFileAsString(file1);
        //System.out.println(json);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
        JSONObject dTree = (JSONObject) jsonObject.get("dtree");
        JSONArray statements = (JSONArray) dTree.get("statements");
        JSONObject node = (JSONObject) statements.get(0);
        JSONObject leaf_node = (JSONObject) node.get("leaf_node");
        JSONArray exp_stack = (JSONArray) leaf_node.get("exp_stack");
        for (int i = 0; i < exp_stack.size(); i++) {
            JSONObject expObject = (JSONObject) exp_stack.get(i);
            //System.out.println(expObject);
            String nudge_title = (String) expObject.get("title");
            System.out.println(nudge_title);
            if (nudge_title.equals(nudgeName)) {
                JSONArray actions = (JSONArray) expObject.get("actions");
                System.out.println(actions);
                JSONObject actionNode = (JSONObject) actions.get(0);
                JSONObject display_prompt = (JSONObject) actionNode.get("display_prompt");
                changes = display_prompt.get("changes");
            }
        }
        return changes;
    }

    public static GetNudgeParser getButton1Key(String btnKey, String filePath, String nudgeName ) throws Exception {
        Long fontSize = null;
        Long cornerRadius = null;
        String textColor = null;
        String text = null;
        String imgName = null;
        String height =null;
        String fontStyle = null;
        String fontFamily = null;
        String borderColor = null;
        String bgColor = null;
        Object changes = getNudgeParser(filePath, nudgeName);
        JSONObject changes1 = new JSONObject((Map) changes);
        JSONObject btn1 = (JSONObject) ((Map) changes).get("btn1");
        System.out.println(btn1);
        JSONObject btn1props = (JSONObject) btn1.get("props");
        if (btnKey.equals("fontSize")) {
            fontSize = (Long) btn1props.get(btnKey);
            System.out.println(fontSize);
        } else if (btnKey.equals("cornerRadius")) {
             cornerRadius = (Long) btn1props.get(btnKey);
        } else if (btnKey.equals("textColor")) {
             textColor = (String) btn1props.get(btnKey);
        } else if (btnKey.equals("text")) {
             text = (String) btn1props.get(btnKey);
        } else if (btnKey.equals("imgName")) {
             imgName = (String) btn1props.get(btnKey);
        } else if (btnKey.equals("height")) {
             height = (String) btn1props.get(btnKey);
        } else if (btnKey.equals("fontStyle")) {
             fontStyle = (String) btn1props.get(btnKey);
        } else if (btnKey.equals("fontFamily")){
             fontFamily = (String) btn1props.get(btnKey);
         }else  if (btnKey.equals("borderColor")) {
             borderColor = (String) btn1props.get(btnKey);
        }else if (btnKey.equals("bgColor")) {
             bgColor = (String) btn1props.get(btnKey);
        }else {
            System.out.println("property not available");
        }

        return new GetNudgeParser(fontSize, cornerRadius, textColor, text, imgName, height, fontStyle,fontFamily, borderColor, bgColor);
    }
    public static GetNudgeParser getButton2Key(String btn2Key, String filePath, String nudgeName ) throws Exception {
        Long fontSize = null;
        String actionType = null;
        Long cornerRadius = null;
        String textColor = null;
        String text = null;
        String imgName = null;
        String height =null;
        String fontStyle = null;
        String fontFamily = null;
        String borderColor = null;
        String bgColor = null;
        String textVariables = null;
        String type = null;
        Object changes = getNudgeParser(filePath, nudgeName);
        JSONObject changes1 = new JSONObject((Map) changes);
        JSONObject btn1 = (JSONObject) ((Map) changes).get("btn2");
        System.out.println(btn1);
        JSONObject btn1props = (JSONObject) btn1.get("props");
        if (btn2Key.equals("fontSize")) {
            fontSize = (Long) btn1props.get(btn2Key);
            System.out.println(fontSize);
        } else if (btn2Key.equals("cornerRadius")) {
            cornerRadius = (Long) btn1props.get(btn2Key);
        } else if (btn2Key.equals("textColor")) {
            textColor = (String) btn1props.get(btn2Key);
        } else if (btn2Key.equals("text")) {
            text = (String) btn1props.get(btn2Key);
        } else if (btn2Key.equals("imgName")) {
            imgName = (String) btn1props.get(btn2Key);
        } else if (btn2Key.equals("height")) {
            height = (String) btn1props.get(btn2Key);
        } else if (btn2Key.equals("fontStyle")) {
            fontStyle = (String) btn1props.get(btn2Key);
        } else if (btn2Key.equals("fontFamily")){
            fontFamily = (String) btn1props.get(btn2Key);
        }else  if (btn2Key.equals("borderColor")) {
            borderColor = (String) btn1props.get(btn2Key);
        }else if (btn2Key.equals("bgColor")) {
            bgColor = (String) btn1props.get(btn2Key);
        }else if (btn2Key.equals("textVariables")){
            textVariables = (String) btn1props.get("textVariables");
        }else if(btn2Key.equals("actionType")){
            actionType = (String) btn1props.get("actionType");
        }else if (btn2Key.equals("type")){
            type = (String) btn1props.get("type");
        }else {
            System.out.println("property not available");
        }

        return new GetNudgeParser(fontSize, actionType, type, cornerRadius, textColor, text, imgName, height, fontStyle,fontFamily, borderColor, bgColor, textVariables);
    }
    public static GetNudgeParser getLabel1Key(String label1Key, String filePath, String nudgeName ) throws Exception {
        Long fontSize = null;
        String textVariables = null;
        String textColor = null;
        String text = null;
        String spacing = null;
        String fontStyle = null;
        String fontFamily = null;
        Object changes = getNudgeParser(filePath, nudgeName);
        JSONObject changes1 = new JSONObject((Map) changes);
        JSONObject label1 = (JSONObject) ((Map) changes).get("label1");
        //System.out.println(label1);
        JSONObject btn1props = (JSONObject) label1.get("props");
        if (label1Key.equals("textVariables")) {
            textVariables = (String) btn1props.get(label1Key);
        } else if (label1Key.equals("text")) {
            text = (String) btn1props.get(label1Key);
        } else if (label1Key.equals("textColor")) {
            textColor = (String) btn1props.get(label1Key);
        }
        else if (label1Key.equals("spacing")) {
            spacing = (String) btn1props.get(label1Key);
        } else if (label1Key.equals("fontStyle")) {
            fontStyle = (String) btn1props.get(label1Key);
        } else if (label1Key.equals("fontSize")) {
            fontSize = (Long) btn1props.get(label1Key);
        } else if (label1Key.equals("fontFamily")){
            fontFamily = (String) btn1props.get(label1Key);
        }else {
            System.out.println("property not available");
        }

        return new GetNudgeParser(textVariables, spacing, textColor, fontSize, text, fontStyle,fontFamily);
    }
    public static GetNudgeParser getPromptKey(String promptKey, String filePath, String nudgeName ) throws Exception {
        String width = null;
        String spacing = null;
        Boolean showDismissErrorMessage = null;
        Boolean showBtnXOrDismissConditionError = null;
        String position = null;
        String maxWidth = null;
        String maxHeight = null;
        Long margin = null;
        Long cornerRadius = null;
        String btnGap = null;
        String btnContainerSpacing = null;
        String borderColor = null;
        Long bgOpacity = null;
        String bgColor = null;
        Double backDropOpacity = null;
        Boolean backdropDismiss = null;
        String backDropColor = null;
        Boolean allowAppInteraction = null;
        Object changes = getNudgeParser(filePath, nudgeName);
        JSONObject changes1 = new JSONObject((Map) changes);
        JSONObject prompt = (JSONObject) ((Map) changes).get("prompt");
        System.out.println(prompt);
        JSONObject promptProps = (JSONObject) prompt.get("props");
        if (promptKey.equals("width")) {
            width = (String) promptProps.get(promptKey);
        }else if (promptKey.equals("spacing")){
            spacing = (String) promptProps.get(promptKey);
        }
        else if (promptKey.equals("showDismissErrorMessage")) {
            showDismissErrorMessage = (Boolean) promptProps.get(promptKey);
        } else if (promptKey.equals("showBtnXOrDismissConditionError")) {
            showBtnXOrDismissConditionError = (Boolean) promptProps.get(promptKey);
        } else if (promptKey.equals("position")) {
            position = (String) promptProps.get(promptKey);
        } else if (promptKey.equals("maxWidth")) {
            maxWidth = (String) promptProps.get(promptKey);
        } else if (promptKey.equals("maxHeight")) {
            maxHeight = (String) promptProps.get(promptKey);
        } else if (promptKey.equals("margin")){
            margin = (Long) promptProps.get(promptKey);
        }else if (promptKey.equals("cornerRadius")){
            cornerRadius = (Long) promptProps.get(promptKey);
        }else if (promptKey.equals("btnGap")){
            btnGap = (String) promptProps.get(promptKey);
        }else if (promptKey.equals("btnContainerSpacing")){
            btnContainerSpacing = (String) promptProps.get(promptKey);
        }else if (promptKey.equals("borderColor")){
            borderColor = (String) promptProps.get(promptKey);
        }else if (promptKey.equals("bgOpacity")){
            bgOpacity = (Long) promptProps.get(promptKey);
        }else if (promptKey.equals("bgColor")){
            bgColor = (String) promptProps.get(promptKey);
        }else if (promptKey.equals("backDropOpacity")){
            backDropOpacity = (Double) promptProps.get(promptKey);
        }else if (promptKey.equals("backdropDismiss")){
            backdropDismiss = (Boolean) promptProps.get(promptKey);
        }else if (promptKey.equals("backDropColor")){
            backDropColor = (String) promptProps.get(promptKey);
        }else if (promptKey.equals("allowAppInteraction")){
            allowAppInteraction = (Boolean) promptProps.get(promptKey);
        }
        else {
            System.out.println("property not available");
        }

        return new GetNudgeParser(width, spacing, showDismissErrorMessage, showBtnXOrDismissConditionError, position, maxWidth, maxHeight, margin, cornerRadius, btnGap, btnContainerSpacing, borderColor, bgOpacity, bgColor, backDropOpacity, backdropDismiss, backDropColor, allowAppInteraction);
    }
    public static GetNudgeParser getLabel2Key(String label2Key, String filePath, String nudgeName ) throws Exception {
        Long fontSize = null;
        String textVariables = null;
        String textColor = null;
        String text = null;
        String spacing = null;
        String fontStyle = null;
        String fontFamily = null;
        Object changes = getNudgeParser(filePath, nudgeName);
        JSONObject changes1 = new JSONObject((Map) changes);
        JSONObject label2 = (JSONObject) ((Map) changes).get("label2");
        System.out.println(label2);
        JSONObject btn1props = (JSONObject) label2.get("props");
        if (label2Key.equals("textVariables")) {
            textVariables = (String) btn1props.get(label2Key);
        } else if (label2Key.equals("text")) {
            text = (String) btn1props.get(label2Key);
        } else if (label2Key.equals("textColor")) {
            textColor = (String) btn1props.get(label2Key);
        } else if (label2Key.equals("spacing")) {
            spacing = (String) btn1props.get(label2Key);
        } else if (label2Key.equals("fontStyle")) {
            fontStyle = (String) btn1props.get(label2Key);
        } else if (label2Key.equals("fontSize")) {
            fontSize = (Long) btn1props.get(label2Key);
        } else if (label2Key.equals("fontFamily")){
            fontFamily = (String) btn1props.get(label2Key);
        }else {
            System.out.println("property not available");
        }

        return new GetNudgeParser(textVariables, spacing, textColor, fontSize, text, fontStyle,fontFamily);
    }
    public static GetNudgeParser getLabel0Key(String label0Key, String filePath, String nudgeName ) throws Exception {
        Long fontSize = null;
        String textVariables = null;
        String textColor = null;
        String text = null;
        String spacing = null;
        String fontStyle = null;
        String fontFamily = null;
        Object changes = getNudgeParser(filePath, nudgeName);
        JSONObject changes1 = new JSONObject((Map) changes);
        JSONObject label0 = (JSONObject) ((Map) changes).get("label2");
        System.out.println(label0);
        JSONObject btn1props = (JSONObject) label0.get("props");
        if (label0Key.equals("textVariables")) {
            textVariables = (String) btn1props.get(label0Key);
        } else if (label0Key.equals("text")) {
            text = (String) btn1props.get(label0Key);
        } else if (label0Key.equals("textColor")) {
            textColor = (String) btn1props.get(label0Key);
        } else if (label0Key.equals("spacing")) {
            spacing = (String) btn1props.get(label0Key);
        } else if (label0Key.equals("fontStyle")) {
            fontStyle = (String) btn1props.get(label0Key);
        } else if (label0Key.equals("fontSize")) {
            fontSize = (Long) btn1props.get(label0Key);
        } else if (label0Key.equals("fontFamily")){
            fontFamily = (String) btn1props.get(label0Key);
        }else {
            System.out.println("property not available");
        }

        return new GetNudgeParser(textVariables, spacing, textColor, fontSize, text, fontStyle,fontFamily);
    }
    public static GetNudgeParser getImage1Key(String img1Key, String filePath, String nudgeName ) throws Exception {
        String height = null;
        Double imgAspectRatio = null;
        Long maxTopSpacing = null;
        Long opacity = null;
        String spacing = null;
        String trackKey = null;
        String url = null;
        String width = null;
        Object changes = getNudgeParser(filePath, nudgeName);
        JSONObject changes1 = new JSONObject((Map) changes);
        JSONObject img1 = (JSONObject) ((Map) changes).get("img1");
        System.out.println(img1);
        JSONObject btn1props = (JSONObject) img1.get("props");
        if (img1Key.equals("height")) {
            height = (String) btn1props.get(img1Key);
        } else if (img1Key.equals("imgAspectRatio")) {
            imgAspectRatio = (Double) btn1props.get(img1Key);
        } else if (img1Key.equals("maxTopSpacing")) {
            maxTopSpacing = (Long) btn1props.get(img1Key);
        } else if (img1Key.equals("spacing")) {
            spacing = (String) btn1props.get(img1Key);
        } else if (img1Key.equals("opacity")) {
            opacity = (Long) btn1props.get(img1Key);
        } else if (img1Key.equals("trackKey")) {
            trackKey = (String) btn1props.get(img1Key);
        } else if (img1Key.equals("url")){
            url = (String) btn1props.get(img1Key);
        }else if(img1Key.equals("width")){
            width = (String) btn1props.get("width");
        } else {
            System.out.println("property not available");
        }
        return new GetNudgeParser(height, imgAspectRatio,maxTopSpacing, spacing, opacity, trackKey, url, width);
    }



    public static void main(String[] args) throws Exception {
        GetNudgeParser getNudgeParser = getImage1Key("maxTopSpacing", "src/test/jsonFile/announcementNudgeJourney.json", "Announcement - 4");
        Long maxTopSpacing = getNudgeParser.maxTopSpacing;
        System.out.println(maxTopSpacing);




    }

}