package Utility;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.github.romankh3.image.comparison.model.ImageComparisonState;
import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.ByteArrayInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.testng.AssertJUnit.assertEquals;

public class CompareImages extends ExtentReport {


    public  static void compareImages(String expected, String actual) throws Exception {
        //load images to be compared:
        Image image1 = Toolkit.getDefaultToolkit().getImage(expected);
        Image image2 = Toolkit.getDefaultToolkit().getImage(actual);

        try {
            BufferedImage expectedImage = ImageComparisonUtil.readImageFromResources(expected);
            BufferedImage actualImage = ImageComparisonUtil.readImageFromResources(actual);
            //String fi = "src/test/screenShots/objectScreenShots/comparedImage.png";
            String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
            //after execution, you could see a folder "FailedTestsScreenshots" under src folder
            String destination = "src/test/screenShots/objectScreenShots/"+ "comparedImage" +dateName+".png";
            // where to save the result (leave null if you want to see the result in the UI)
            File resultDestination = new File(destination);
            //Create ImageComparison object with result destination and compare the images.
            ImageComparisonResult imageComparisonResult = new ImageComparison(expectedImage, actualImage, resultDestination).compareImages();
            System.out.println(imageComparisonResult.getResult());
            PixelGrabber grabImage1Pixels = new PixelGrabber(image1, 0, 0, -1,
                    -1, false);
            PixelGrabber grabImage2Pixels = new PixelGrabber(image2, 0, 0, -1,
                    -1, false);

            int[] image1Data = null;

            if (grabImage1Pixels.grabPixels()) {
                int width = grabImage1Pixels.getWidth();
                int height = grabImage1Pixels.getHeight();
                image1Data = new int[width * height];
                image1Data = (int[]) grabImage1Pixels.getPixels();
            }

            int[] image2Data = null;

            if (grabImage2Pixels.grabPixels()) {
                int width = grabImage2Pixels.getWidth();
                int height = grabImage2Pixels.getHeight();
                image2Data = new int[width * height];
                image2Data = (int[]) grabImage2Pixels.getPixels();
            }
            System.out.println("Pixels equal: "
                    + java.util.Arrays.equals(image1Data, image2Data));
            if (java.util.Arrays.equals(image1Data, image2Data) == true) {
                test.log(LogStatus.PASS, "image are matching");
            }else{
                Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
                test.log(LogStatus.INFO,test.addScreenCapture(getFailScreenshot(driver, "mismatchImage")));
                }

        } catch (InterruptedException e1) {
            e1.printStackTrace();
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            test.log(LogStatus.INFO,test.addScreenCapture(getFailScreenshot(driver, "mismatchImage")));

        } catch (Exception e) {
            e.printStackTrace();
        }
       /* if (imageComparisonResult.getImageComparisonState().equals(ImageComparisonState.MATCH)){
            System.out.println("image matched, no difference");
            //test.log(LogStatus.PASS, "image matched, no difference");
        }else {
            System.out.println("image didn't match, difference");
            //test.log(LogStatus.ERROR,"image did not match, difference");
        }*/

    }
    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] screenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

}

