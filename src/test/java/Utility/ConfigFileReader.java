package Utility;

import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
    Environment testEnvironment;
    private Properties properties;
    //String propertyFilePath = "src/main/resources/prod.properties";

//    @BeforeTest
//    @Parameters({"environment"})
//    public String beforeMethod(String environment) {
//        ConfigFactory.setProperty("env", environment);
//        testEnvironment = ConfigFactory.create(Environment.class);
//         String propertyFilePath= "src/main/resources/{env}.properties";
//        System.out.println(propertyFilePath);
//         return propertyFilePath;
//    }
    public ConfigFileReader() {
        BufferedReader reader;
        try {
            String jenkinsVariableProd = System.getProperty("prod");
            System.out.println(jenkinsVariableProd);
            String jenkinsVariableStage = System.getProperty("stage");
            if(("prod").equals(jenkinsVariableProd)) {
                String propertyFilePath = "src/main/resources/" + jenkinsVariableProd + ".properties";
                reader = new BufferedReader(new FileReader(propertyFilePath));
                properties = new Properties();
                try {
                    properties.load(reader);
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else if(("stage").equals(jenkinsVariableStage)){
                String propertyFilePath = "src/main/resources/" + jenkinsVariableStage + ".properties";
                reader = new BufferedReader(new FileReader(propertyFilePath));
                properties = new Properties();
                try {
                    properties.load(reader);
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                String propertyFilePath = "src/main/resources/prod.properties";
                reader = new BufferedReader(new FileReader(propertyFilePath));
                properties = new Properties();
                try {
                    properties.load(reader);
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at ");
        }
    }
    public String getDriverPath(){
        String driverPath = properties.getProperty("driverPath");
        if(driverPath!= null) return driverPath;
        else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");
    }
    public String getAppId(){
        String appId = properties.getProperty("appId");
        System.out.println(appId);
        if(appId!= null) return appId;
        else throw new RuntimeException("appId not specified in the Configuration.properties file.");
    }
    public String getCompanyEmail() {
        String company_email = properties.getProperty("company_email");
        if(company_email != null) return company_email;
        else throw new RuntimeException("company_email not specified in the Configuration.properties file.");
    }
    public String getApplicationUrl() {
        String url = properties.getProperty("url");
        if(url != null) return url;
        else throw new RuntimeException("url not specified in the Configuration.properties file.");
    }
    public String getCompanyPassword() {
        String company_password = properties.getProperty("company_password");
        if(company_password != null) return company_password;
        else throw new RuntimeException("company_password not specified in the Configuration.properties file.");
    }
}
