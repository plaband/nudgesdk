package Utility;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

public class AdbLog {
    public static AndroidDriver driver;
    public String getNudgeLog(String nudgeName) throws IOException {
        LogEntries logTypes = driver.manage().logs().get("logcat");
        List<LogEntry> logs = logTypes.getAll();
        String nudgeLog = "";
        for (LogEntry logEntry : logs) {
            if (logEntry.getMessage().contains("Debug Logs")) {
                nudgeLog = logEntry.getMessage();
                System.out.println(logEntry.getMessage());
                File fileName = new File("src/test/jsonFile/log.text");
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(nudgeLog);
                writer.close();
            }

        }
        return nudgeLog;
    }


}
