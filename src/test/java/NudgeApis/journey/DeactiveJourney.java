package NudgeApis.journey;
import Utility.ConfigFileReader;
import Utility.Environment;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import NudgeApis.login;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import static io.restassured.RestAssured.given;
public class DeactiveJourney extends ExtentReport {
    Environment testEnvironment;
    String session_id = login.login();
    ConfigFileReader configFileReader = new ConfigFileReader();
    final String ROOT_URI = configFileReader.getApplicationUrl();
    String appId = configFileReader.getAppId();
    @Test
    public Boolean deactivateJourney() {
        Boolean deactivateJourney = null;
        try {
            RestAssured.useRelaxedHTTPSValidation();
            Map<String, Object> headerMap = new HashMap<String, Object>();
            headerMap.put("Content-Type", "application/json");
            headerMap.put("Accept", "application/json");
            String jsonBody = "{}";
            InputStream input = new FileInputStream("src/test/jsonFile/Ids.properties");
            Properties prop = new Properties();
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            String journeyId = prop.getProperty("journey_id");
            Response response = given()
                    .cookies("sessionid", session_id)
                    .pathParam("journey_id", journeyId)
                    .pathParam("appId", appId)
                    .headers(headerMap)
                    .body(jsonBody)
                    .when()
                    .post(ROOT_URI + "/ujm/v1/{appId}/{journey_id}/d");
            //String jsonData = response.asString();
            //System.out.println(jsonData);
            JsonPath jsonPathEvaluator = response.jsonPath();
            deactivateJourney = jsonPathEvaluator.get("is_error");
            if (deactivateJourney.equals(false)) {
                test.log(LogStatus.PASS,"Journey is deactivated");
                System.out.println("value is equal to false");

            }
        } catch (Exception e) {
            test.log(LogStatus.FAIL,"Journey deactivate failed");
            e.printStackTrace();
        }
        return deactivateJourney;
    }
}
