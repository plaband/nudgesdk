package NudgeApis.journey;

import Utility.ConfigFileReader;
import Utility.Environment;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import NudgeApis.login;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class CreateJourney extends ExtentReport {
    Environment testEnvironment;
    String session_id = login.login();
    ConfigFileReader configFileReader = new ConfigFileReader();
    final String ROOT_URI = configFileReader.getApplicationUrl();
    String appId = configFileReader.getAppId();
    @Test
    public String createJourney(String path) {
        String journey_id = null;
        try {
            RestAssured.useRelaxedHTTPSValidation();
            Map<String, Object> headerMap = new HashMap<String, Object>();
            headerMap.put("Content-Type", "application/json");
            headerMap.put("Accept", "application/json");
            File jsonBody = new File(path);

            Response response = given()
                                .cookies("sessionid", session_id)
                                .headers(headerMap)
                                .body(jsonBody)
                                .when()
                                .post(ROOT_URI + "/ujm/v1");
            //String jsonData = response.asString();
            //System.out.println(jsonData);
            JsonPath jsonPathEvaluator = response.jsonPath();
            journey_id = jsonPathEvaluator.getString("api_response.journey_id");
            System.out.println(journey_id);
            test.log(LogStatus.PASS, "Journey gets created and id is:", journey_id );

        } catch (Exception e) {
            test.log(LogStatus.FAIL, "Journey Creation Failed");
            e.printStackTrace();
        }

        return journey_id;
    }



}
