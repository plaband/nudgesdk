package NudgeApis.journey;

import Utility.ConfigFileReader;
import Utility.Environment;
import Utility.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import NudgeApis.login;
import org.aeonbits.owner.ConfigFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class AddNudgeJourney extends ExtentReport {
    Environment testEnvironment;
    String session_id = login.login();
    ConfigFileReader configFileReader = new ConfigFileReader();
    final String ROOT_URI = configFileReader.getApplicationUrl();
    String appId = configFileReader.getAppId();
    @Test
    public Boolean addNudgeJourney(String propertyPath, String nudgeJsonPath, String createJourneyJson) throws IOException {
        //Environment testEnvironment = ConfigFactory.create(Environment.class);
        //System.out.println(testEnvironment.company_password());
        Boolean errorCheck = null;
        CreateJourney createJourney = new CreateJourney();
        String journey_id = createJourney.createJourney(createJourneyJson);
        OutputStream output = new FileOutputStream(propertyPath);
        Properties prop = new Properties();
        prop.setProperty("journey_id", journey_id);
        prop.store(output, null);
            JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(nudgeJsonPath))
        {

            //Read JSON file
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            //JSONObject nudgeObject = new JSONObject();
            System.out.println(jsonObject);
            jsonObject.put("jid", journey_id);
            try (FileWriter file = new FileWriter(nudgeJsonPath)) {
                //We can write any JSONArray or JSONObject instance to the file
                file.write(jsonObject.toJSONString());
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

            RestAssured.useRelaxedHTTPSValidation();
            Map<String, Object> headerMap = new HashMap<String, Object>();
            headerMap.put("Content-Type", "application/json");
            headerMap.put("Accept", "application/json");
            File jsonBody = new File(nudgeJsonPath);

            Response response = given()
                    .cookies("sessionid", session_id)
                    .headers(headerMap)
                    .body(jsonBody)
                    .when()
                    .post(ROOT_URI + "/ujm/v1");
            //String jsonData = response.asString();
            //System.out.println(jsonData);
            JsonPath jsonPathEvaluator = response.jsonPath();
            errorCheck = jsonPathEvaluator.get("is_error");
            if(errorCheck.equals(false)){
                test.log(LogStatus.PASS, "Nudges are added in the journey");
                System.out.println("value is equal to false");

            }


        }
        catch (Exception e) {
            test.log(LogStatus.FAIL, "Nudge addition is failed for journey");
            e.printStackTrace();
        }
        return errorCheck;
    }

}
