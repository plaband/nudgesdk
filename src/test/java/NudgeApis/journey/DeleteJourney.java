package NudgeApis.journey;

import Utility.ConfigFileReader;
import Utility.Environment;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import NudgeApis.login;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class DeleteJourney {
    Environment testEnvironment;
    String session_id = login.login();
    ConfigFileReader configFileReader = new ConfigFileReader();
    final String ROOT_URI = configFileReader.getApplicationUrl();
    String appId = configFileReader.getAppId();
    @Test
    public void deleteJourney() {
        try {
            RestAssured.useRelaxedHTTPSValidation();
            Map<String, Object> headerMap = new HashMap<String, Object>();
            headerMap.put("Content-Type", "application/json");
            headerMap.put("Accept", "application/json");
            String jsonBody = "{}";
            InputStream input = new FileInputStream("src/test/jsonFile/Ids.properties");
            Properties prop = new Properties();
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            String journeyId = prop.getProperty("journey_id");
            Response response = given()
                    .cookies("sessionid", session_id)
                    .pathParam("journey_id", journeyId)
                    .pathParam("appId", appId)
                    .headers(headerMap)
                    .body(jsonBody)
                    .when()
                    .delete(ROOT_URI + "/ujm/v1/{appId}/{journey_id}");
            String jsonData = response.asString();
            System.out.println(jsonData);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
