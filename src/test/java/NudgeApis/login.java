package NudgeApis;

import Utility.ConfigFileReader;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class login {
    //final static String ROOT_URI = "https://console.hansel.io/dashboard";
    @Test
    public static String login(){
        ConfigFileReader configFileReader = new ConfigFileReader();
        final String ROOT_URI = configFileReader.getApplicationUrl();
        RestAssured.useRelaxedHTTPSValidation();
        Response response = given().
                contentType(ContentType.URLENC)
                .accept(ContentType.JSON)
                .param("company_email", configFileReader.getCompanyEmail())
                .param("company_password", configFileReader.getCompanyPassword())
                .when()
                .post(ROOT_URI + "/login");
        System.out.println("POST Response\n" + response.asString());

        response.then().body("is_error", Matchers.is(false));
        String session_id = response.getCookie("sessionid");

        String jsonData = response.asString();

        //Assert.assertEquals(jsonData.contains("is_error"), true);

        JsonPath jsonPathEvaluator = response.jsonPath();

        Boolean errorCheck = jsonPathEvaluator.get("is_error");

        if(errorCheck.equals(false)){

            System.out.println("value is equal to false");

        }

        return session_id;

    }

}
