package pCloudyController.Helper;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Base {
    public AndroidDriver<AndroidElement> driver;

    @Parameters({"deviceName"})
    @BeforeTest
    public void beforeTest(String deviceName) throws MalformedURLException {
        //System.out.println(deviceName);
        File fileToBeUploaded = new File("./miltongreen-nonproguarded-userjourney.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("pCloudy_Username", "plaban.dwivedy@netcore.co.in");
        capabilities.setCapability("pCloudy_ApiKey", "kfznxt93m3j3dtrs5jgqgpyk");
        capabilities.setCapability("pCloudy_ApplicationName", "miltongreen-nonproguarded-userjourney.apk");
        capabilities.setCapability("pCloudy_DurationInMinutes", 20);
        capabilities.setCapability("pCloudy_DeviceManafacturer", deviceName);
        capabilities.setCapability("appPackage", "io.hansel.sampleapp.userjourney.miltongreen");
        //caps.setCapability("appActivity","io.visualizerapp.app.MainActivity");
        capabilities.setCapability("appWaitActivity","com.company.miltongreen.activity.LaunchActivity");
        driver = new AndroidDriver(new URL("https://device.pcloudy.com/appiumcloud/wd/hub"), capabilities);
    }
}
