package pCloudyController.Tests;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.Promotion2NudgeObject;
import PageObject.NudgeObject.Promotion4NudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.NudgeMap;
import pCloudyController.Controller;
import pCloudyController.Helper.Base;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase5_PromotionNudge extends ExtentReport {
    //public static AndroidDriver driver;
    Controller controller = new Controller();
    @Test(priority = 24)
    @Step("Start A Journey for Promotion nudges")
    @Description("Start A Journey for Promotion nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/promotionNudgeJourney.json", "src/test/jsonFile/createPromotionJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(priority = 25)
    @Description("Save custom Event")
    @Severity(SeverityLevel.MINOR)
    public void saveCustomEvent() throws Exception {
        WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
        welcomeNudge.resetApp();
        Thread.sleep(2000);
        //Select pop up
        welcomeNudge.envScrollMethod();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(1000);
        welcomeNudge.appNameSelect();
        Thread.sleep(1000);
        welcomeNudge.popUpSelect();
        Thread.sleep(1000);
        welcomeNudge.resetApp();
        Thread.sleep(3000);
        //welcomeNudge.launchApp();
        //Second app launch
        welcomeNudge.envScrollMethod();
        Thread.sleep(3000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(3000);
        welcomeNudge.appNameSelect();
        Thread.sleep(3000);
        welcomeNudge.popUpSelect();
        welcomeNudge.terminateApp();
        welcomeNudge.launchApp();
        //wno.app_foreGround();
        //Select MiltonGreen App
        welcomeNudge.miltonGreenBtn_click();
        Thread.sleep(3000);
        //welcomeNudge.scrollAndClick("FIRE CUSTOM EVENT");
        welcomeNudge.clickFireCustomEvent();
        Thread.sleep(3000);
        welcomeNudge.selectVendorSpinner();
        Thread.sleep(3000);
        welcomeNudge.selectSmartTech();
        Thread.sleep(3000);
        welcomeNudge.enterEventName("Seg-Test");
        welcomeNudge.selectSavePncEvent();
        Thread.sleep(3000);
        welcomeNudge.pressBackButton();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "POINT AND CLICK");
        welcomeNudge.pointClickBtn_click();
        Thread.sleep(3000);

    }
    @Test(description = "Promotion 4 nudge testCase", priority = 26)
    @Description("Promotion 4 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert1Nudge() throws InterruptedException, IOException {
        try {
            Promotion4NudgeObject promotion4 = new Promotion4NudgeObject(driver);
            promotion4.fireEventBtn_click();
            Thread.sleep(3000);
            promotion4.getPromotion4Log();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Promotion -4");
            Thread.sleep(3000);
            promotion4.getLabel1Text();
            Thread.sleep(3000);
            promotion4.getLabe2Text();
            Thread.sleep(3000);
            promotion4.getBtn1Text();
            Thread.sleep(3000);
            promotion4.getBtn2Text();
            Thread.sleep(3000);
            promotion4.promotion4NudgeObjectScreenShot();
            Thread.sleep(3000);
            promotion4.nudgeMatch();
            Thread.sleep(3000);
            promotion4.clickBtn2();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Test(description = "Promotion 2 nudge testCase", priority = 27)
    @Description("Promotion 2 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert2Nudge() throws InterruptedException, IOException {
        try {
            Promotion2NudgeObject promotion2 = new Promotion2NudgeObject(driver);
            promotion2.getPromotion2Log();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Promotion -2");
            promotion2.getLabelHText();
            Thread.sleep(3000);
            promotion2.getImgHText();
            Thread.sleep(3000);
            promotion2.getBtn1Text();
            Thread.sleep(3000);
            promotion2.getBtn2Text();
            Thread.sleep(3000);
            promotion2.promotion2NudgeObjectScreenShot();
            Thread.sleep(3000);
            promotion2.nudgeMatch();
            Thread.sleep(3000);
            promotion2.clickBtn2();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete Promotion nudge journey")
    @Test(priority = 28)
    @Description("Delete Promotion nudge journey")
    @Severity(SeverityLevel.BLOCKER)
    public void deletePromotionJourney() throws Exception {
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        Thread.sleep(2000);
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();

    }
}
