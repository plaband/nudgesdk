package pCloudyController.Tests;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.Alert1NudgeObject;
import PageObject.NudgeObject.Alert2NudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.NudgeMap;
import pCloudyController.Controller;
import pCloudyController.Helper.Base;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase4_AlertNudge extends ExtentReport {
    Controller controller = new Controller();
    //public static AndroidDriver driver;
    @Test(priority = 19)
    @Step("Start A Journey for Alert nudges")
    @Description("Start A Journey for Alert nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/alertNudgeJourney.json", "src/test/jsonFile/createAlertJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(priority = 20)
    @Description("Save custom Event")
    @Severity(SeverityLevel.MINOR)
    public void saveCustomEvent() throws Exception {
        WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
        welcomeNudge.resetApp();
        Thread.sleep(2000);
        //Select pop up
        welcomeNudge.envScrollMethod();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(1000);
        welcomeNudge.appNameSelect();
        Thread.sleep(1000);
        welcomeNudge.popUpSelect();
        Thread.sleep(1000);
        welcomeNudge.resetApp();
        Thread.sleep(3000);
        //welcomeNudge.launchApp();
        //Second app launch
        welcomeNudge.envScrollMethod();
        Thread.sleep(3000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(3000);
        welcomeNudge.appNameSelect();
        Thread.sleep(3000);
        welcomeNudge.popUpSelect();
        welcomeNudge.terminateApp();
        welcomeNudge.launchApp();
        //wno.app_foreGround();
        //Select MiltonGreen App
        welcomeNudge.miltonGreenBtn_click();
        Thread.sleep(3000);
        //welcomeNudge.scrollAndClick("FIRE CUSTOM EVENT");
        welcomeNudge.clickFireCustomEvent();
        Thread.sleep(3000);
        welcomeNudge.selectVendorSpinner();
        Thread.sleep(3000);
        welcomeNudge.selectSmartTech();
        Thread.sleep(3000);
        welcomeNudge.enterEventName("Seg-Test");
        welcomeNudge.selectSavePncEvent();
        Thread.sleep(3000);
        welcomeNudge.pressBackButton();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "POINT AND CLICK");
        welcomeNudge.pointClickBtn_click();
        Thread.sleep(3000);

    }
    @Test(description = "Alert 1 nudge testCase", priority = 21)
    @Description("Alert 1 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert1Nudge() throws InterruptedException, IOException {
        try {
            Alert1NudgeObject alert1 = new Alert1NudgeObject(driver);
            alert1.fireEventBtn_click();
            Thread.sleep(3000);
            alert1.getAlert1Log();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Alert -1");
            Thread.sleep(3000);
            alert1.getLabel1Text();
            Thread.sleep(3000);
            alert1.Alert1NudgeObjectScreenShot();
            Thread.sleep(3000);
            alert1.nudgeMatch();
            Thread.sleep(3000);
            alert1.clickBtnX();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Test(description = "Alert 2 nudge testCase", priority = 22)
    @Description("Alert 2 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void alert2Nudge() throws InterruptedException, IOException {
        try {
            Alert2NudgeObject alert2 = new Alert2NudgeObject(driver);
            alert2.getAlert2Log();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Alert -2");
            alert2.getLabel1Text();
            Thread.sleep(3000);
            alert2.getBtn1Text();
            Thread.sleep(3000);
            alert2.Alert2NudgeObjectScreenShot();
            Thread.sleep(3000);
            alert2.nudgeMatch();
            Thread.sleep(3000);
            alert2.clickBtn1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete Alert nudge journey")
    @Test(description = "Delete Alert nudge journey", priority = 23)
    @Description("Delete Alert nudge journey")
    @Severity(SeverityLevel.BLOCKER)
    public void deleteAlertJourney() throws Exception {
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        Thread.sleep(2000);
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();

    }

}
