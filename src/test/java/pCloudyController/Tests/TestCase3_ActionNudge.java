package pCloudyController.Tests;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.Action1NudgeObject;
import PageObject.NudgeObject.Action2NudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.NudgeMap;
import pCloudyController.Controller;
import pCloudyController.Helper.Base;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase3_ActionNudge extends ExtentReport {
    Controller controller = new Controller();
    @Test(priority = 14)
    @Description("Start A Journey for Action nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/actionsNudgeJourney.json", "src/test/jsonFile/createActionsJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(priority = 15)
    @Description("Save custom Event")
    @Severity(SeverityLevel.MINOR)
    public void saveCustomEvent() throws Exception {
        WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
        welcomeNudge.resetApp();
        Thread.sleep(2000);
        //Select pop up
        welcomeNudge.envScrollMethod();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(1000);
        welcomeNudge.appNameSelect();
        Thread.sleep(1000);
        welcomeNudge.popUpSelect();
        Thread.sleep(1000);
        welcomeNudge.resetApp();
        Thread.sleep(3000);
        //welcomeNudge.launchApp();
        //Second app launch
        welcomeNudge.envScrollMethod();
        Thread.sleep(3000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(3000);
        welcomeNudge.appNameSelect();
        Thread.sleep(3000);
        welcomeNudge.popUpSelect();
        welcomeNudge.terminateApp();
        welcomeNudge.launchApp();
        //wno.app_foreGround();
        //Select MiltonGreen App
        welcomeNudge.miltonGreenBtn_click();
        Thread.sleep(3000);
        //welcomeNudge.scrollAndClick("FIRE CUSTOM EVENT");
        welcomeNudge.clickFireCustomEvent();
        Thread.sleep(3000);
        welcomeNudge.selectVendorSpinner();
        Thread.sleep(3000);
        welcomeNudge.selectSmartTech();
        Thread.sleep(3000);
        welcomeNudge.enterEventName("Seg-Test");
        welcomeNudge.selectSavePncEvent();
        Thread.sleep(3000);
        welcomeNudge.pressBackButton();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "POINT AND CLICK");
        welcomeNudge.pointClickBtn_click();
        Thread.sleep(3000);
    }
    @Test(description = "Action 1 nudge testCase", priority = 16)
    @Description("Action 1 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void action1Nudge() throws InterruptedException, IOException {
        try {
            Action1NudgeObject action1 = new Action1NudgeObject(driver);
            action1.fireEventBtn_click();
            Thread.sleep(3000);
            action1.getAction1Log();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Action -1");
            Thread.sleep(3000);
            action1.getLabel1Text();
            Thread.sleep(3000);
            action1.getLabel2Text();
            Thread.sleep(3000);
            action1.getButton1Text();
            Thread.sleep(3000);
            action1.getButton2Text();
            Thread.sleep(3000);
            action1.Action1NudgeObjectScreenShot();
            Thread.sleep(3000);
            action1.nudgeMatch();
            Thread.sleep(3000);
            action1.clickButton1();

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Test(description = "Action 2 nudge testCase", priority = 17)
    @Description("Action 2 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void action2Nudge() throws InterruptedException, IOException {
        try {
            Action2NudgeObject action2 = new Action2NudgeObject(driver);
            action2.getAction2Log();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Action -2");
            Thread.sleep(3000);
            action2.getLabel2Text();
            Thread.sleep(3000);
            action2.getLabel2Text();
            Thread.sleep(3000);
            action2.getButton1Text();
            Thread.sleep(3000);
            action2.Action2NudgeObjectScreenShot();
            Thread.sleep(3000);
            action2.nudgeMatch();
            Thread.sleep(3000);
            action2.clickButton1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete action nudge journey")
    @Test(description = "Delete action nudge journey", priority = 18)
    @Description("Delete action nudge journey")
    @Severity(SeverityLevel.BLOCKER)
    public void deleteActionJourney() throws Exception {
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        Thread.sleep(20000);
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();
        //controller.afterTest();
    }
}
