package pCloudyController.Tests;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.NpsNudgeObject;
import PageObject.NudgeObject.UserFeedbackNudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.NudgeMap;
import io.qameta.allure.*;
import org.junit.experimental.theories.Theories;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import pCloudyController.Controller;
import pCloudyController.Helper.Base;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase2_FeedbackNudge extends ExtentReport {
    //public static AndroidDriver<AndroidElement> driver;
    Controller controller = new Controller();
    @Test(priority = 9)
    @Description("Start A Journey for FeedBack nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/feedbackNudgeJourney.json", "src/test/jsonFile/createFeedbackJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(priority = 10)
    @Description("Save custom Event")
    @Severity(SeverityLevel.MINOR)
    public void saveCustomEvent() throws Exception {
        WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
        welcomeNudge.resetApp();
        Thread.sleep(2000);
        //Select pop up
        welcomeNudge.envScrollMethod();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(1000);
        welcomeNudge.appNameSelect();
        Thread.sleep(1000);
        welcomeNudge.popUpSelect();
        Thread.sleep(1000);
        welcomeNudge.resetApp();
        Thread.sleep(3000);
        //welcomeNudge.launchApp();
        //Second app launch
        welcomeNudge.envScrollMethod();
        Thread.sleep(3000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(3000);
        welcomeNudge.appNameSelect();
        Thread.sleep(3000);
        welcomeNudge.popUpSelect();
        welcomeNudge.terminateApp();
        welcomeNudge.launchApp();
        //wno.app_foreGround();
        //Select MiltonGreen App
        welcomeNudge.miltonGreenBtn_click();
        Thread.sleep(3000);
        //welcomeNudge.scrollAndClick("FIRE CUSTOM EVENT");
        welcomeNudge.clickFireCustomEvent();
        Thread.sleep(3000);
        welcomeNudge.selectVendorSpinner();
        Thread.sleep(3000);
        welcomeNudge.selectSmartTech();
        Thread.sleep(3000);
        welcomeNudge.enterEventName("Seg-Test");
        welcomeNudge.selectSavePncEvent();
        Thread.sleep(3000);
        welcomeNudge.pressBackButton();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "POINT AND CLICK");
        welcomeNudge.pointClickBtn_click();
        Thread.sleep(3000);

    }
    @Test(description = "Net Promoter Score nudge testCase", priority = 11)
    @Description("Net Promoter Score nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void netPromoterScoreNudge() throws InterruptedException, IOException {
        try {
            NpsNudgeObject netPromoterScore = new NpsNudgeObject(driver);
            netPromoterScore.fireEventBtn_click();
            Thread.sleep(3000);
            netPromoterScore.getNpsNudgeLog();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Net Promoter Score");
            Thread.sleep(3000);
            netPromoterScore.getLabel1Text();
            Thread.sleep(1000);
            netPromoterScore.getLabel3Text();
            Thread.sleep(1000);
            netPromoterScore.getNpsLayout();
            Thread.sleep(1000);
            netPromoterScore.getLeastLikelyText();
            Thread.sleep(1000);
            netPromoterScore.getVeryLikelyText();
            netPromoterScore.NpsNudgeScreenShot();
            Thread.sleep(3000);
            netPromoterScore.nudgeMatch();
            Thread.sleep(3000);
            netPromoterScore.getBtn1Text();
            Thread.sleep(1000);
            netPromoterScore.selectRating();
            Thread.sleep(3000);
            netPromoterScore.btn1Click();
            Thread.sleep(2000);

        }catch (Exception e){
            System.out.println(e);
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Test(description = "User FeedBack nudge testCase", priority = 12)
    @Description("User FeedBack nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void userFeedBackNudge() throws InterruptedException, IOException {
        try {
        UserFeedbackNudgeObject userFeedBack = new UserFeedbackNudgeObject(driver);
        userFeedBack.getFeedBackLog();
        Thread.sleep(3000);
        NudgeMap nudgeMap = NudgeMap.label1Key("User Feedback");
        Thread.sleep(3000);
        userFeedBack.getLabel1Text();
        Thread.sleep(1000);
        userFeedBack.getLabel2Text();
        Thread.sleep(1000);
        userFeedBack.getTextView();
        Thread.sleep(1000);
        userFeedBack.getCancelBtn();
        Thread.sleep(1000);
        userFeedBack.getOkBtn();
        Thread.sleep(1000);
        userFeedBack.UserFeedbackNudgeScreenShot();
        Thread.sleep(3000);
        userFeedBack.nudgeMatch();
        Thread.sleep(3000);
        userFeedBack.enterTextView("Hi");
        Thread.sleep(2000);
        userFeedBack.okBtnClick();
        }catch (Exception e){
            System.out.println(e);
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Step("Delete FeedBack nudge journey")
    @Description("Delete FeedBack nudge journey")
    @Test(description = "Delete FeedBack nudge journey", priority = 13)
    @Severity(SeverityLevel.BLOCKER)
    public void  deleteFeedbackJourney() throws Exception {
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        Thread.sleep(2000);
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();

    }

}
