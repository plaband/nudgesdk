package pCloudyController.Tests;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.BeaconWithTextBtnNudgeObject;
import PageObject.NudgeObject.SpotLightWithTextBtnNudgeObject;
import PageObject.NudgeObject.TooltipWithTextBtnNudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.NudgeMap;
import pCloudyController.Controller;
import pCloudyController.Helper.Base;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase6_PncNudge extends ExtentReport {
    //public static AndroidDriver driver;
    Controller controller = new Controller();
    @BeforeTest
    public void open_app() throws Exception {
        controller.prepareTest();
        driver = Controller.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");

    }
    @Test(priority = 29)
    @Step("Start A Journey for PNC nudges")
    @Description("Start A Journey for PNC nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/pncNudgeJourney.json", "src/test/jsonFile/createPncJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(priority = 30)
    @Description("Save custom Event")
    @Severity(SeverityLevel.MINOR)
    public void saveCustomEvent() throws Exception {
        WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
        welcomeNudge.resetApp();
        Thread.sleep(2000);
        //Select pop up
        welcomeNudge.envScrollMethod();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(1000);
        welcomeNudge.appNameSelect();
        Thread.sleep(1000);
        welcomeNudge.popUpSelect();
        Thread.sleep(1000);
        welcomeNudge.resetApp();
        Thread.sleep(3000);
        //welcomeNudge.launchApp();
        //Second app launch
        welcomeNudge.envScrollMethod();
        Thread.sleep(3000);
        welcomeNudge.scrollToAnElementByText(driver, "India_Server");
        Thread.sleep(3000);
        welcomeNudge.appNameSelect();
        Thread.sleep(3000);
        welcomeNudge.popUpSelect();
        welcomeNudge.terminateApp();
        welcomeNudge.launchApp();
        //wno.app_foreGround();
        //Select MiltonGreen App
        welcomeNudge.miltonGreenBtn_click();
        Thread.sleep(3000);
        //welcomeNudge.scrollAndClick("FIRE CUSTOM EVENT");
        welcomeNudge.clickFireCustomEvent();
        Thread.sleep(3000);
        welcomeNudge.selectVendorSpinner();
        Thread.sleep(3000);
        welcomeNudge.selectSmartTech();
        Thread.sleep(3000);
        welcomeNudge.enterEventName("Seg-Test");
        welcomeNudge.selectSavePncEvent();
        Thread.sleep(3000);
        welcomeNudge.pressBackButton();
        Thread.sleep(1000);
        welcomeNudge.scrollToAnElementByText(driver, "POINT AND CLICK");
        welcomeNudge.pointClickBtn_click();
        Thread.sleep(3000);
    }
    @Test(description = "Tooltip - Text, Button nudge testCase", priority = 31)
    @Description("Tooltip - Text, Button nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void toolTipNudge() throws InterruptedException, IOException {
        try {
            TooltipWithTextBtnNudgeObject tooltip = new TooltipWithTextBtnNudgeObject(driver);
            tooltip.clickAllRepeatable();
            Thread.sleep(3000);
            tooltip.clickListInList();
            Thread.sleep(3000);
            tooltip.fireEventBtn_click();
            Thread.sleep(3000);
            tooltip.getTooltipWithTextBtnLog();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Tooltip - Text, Button");
            Thread.sleep(3000);
            tooltip.getLabel1Text();
            Thread.sleep(3000);
            tooltip.getBtn1Text();
            Thread.sleep(3000);
            tooltip.TooltipWithTextBtnNudgeObjectScreenShot();
            Thread.sleep(3000);
            tooltip.nudgeMatch();
            Thread.sleep(3000);
            tooltip.clickBtn1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));
        }
    }
    @Test(description = "Beacon - Text, Button nudge testCase", priority = 32)
    @Description("Beacon - Text, Button nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void beaconNudge() throws InterruptedException, IOException {
        try {
            BeaconWithTextBtnNudgeObject beacon = new BeaconWithTextBtnNudgeObject(driver);
            beacon.getBeaconWithTextBtnLog();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Beacon - Text, Button");
            Thread.sleep(3000);
            beacon.getLabel1Text();
            Thread.sleep(3000);
            beacon.getBtn1Text();
            Thread.sleep(3000);
            beacon.BeaconWithTextBtnNudgeObjectScreenShot();
            Thread.sleep(3000);
            beacon.nudgeMatch();
            Thread.sleep(3000);
            beacon.clickBtn1();
            Thread.sleep(3000);
        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Test(description = "Spotlight - Text, Button nudge testCase", priority = 33)
    @Description("Spotlight - Text, Button nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public static void spotLightNudge() throws InterruptedException, IOException {
        try {
            SpotLightWithTextBtnNudgeObject spotLight = new SpotLightWithTextBtnNudgeObject(driver);
            spotLight.getSpotLightWithTextBtnLog();
            Thread.sleep(3000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Spotlight - Text, Button");
            spotLight.getLabel1Text();
            Thread.sleep(3000);
            spotLight.getBtn1Text();
            Thread.sleep(3000);
            spotLight.SpotLightWithTextBtnNudgeObjectScreenShot();
            Thread.sleep(3000);
            spotLight.nudgeMatch();
            Thread.sleep(3000);
            spotLight.clickBtn1();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Step("Delete PNC nudge journey")
    @Test(priority = 34)
    @Description("Delete PNC nudge journey")
    @Severity(SeverityLevel.BLOCKER)
    public void deletePncJourney() throws Exception {
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        Thread.sleep(2000);
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();

    }
    @Step("release session")
    @Severity(SeverityLevel.BLOCKER)
    @Description("release session")
    @AfterTest
    public void afterTest() throws Exception {
        Thread.sleep(2000);
        controller.afterTest();
    }
}
