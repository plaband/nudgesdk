package pCloudyController.Tests;

import NudgeApis.journey.ActiveJourney;
import NudgeApis.journey.AddNudgeJourney;
import NudgeApis.journey.DeactiveJourney;
import NudgeApis.journey.DeleteJourney;
import PageObject.NudgeObject.Announcement2NudgeObject;
import PageObject.NudgeObject.Announcement3NudgeObject;
import PageObject.NudgeObject.Announcement4NudgeObject;
import PageObject.NudgeObject.Announcement5NudgeObject;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.NudgeMap;
import org.json.simple.parser.ParseException;
import pCloudyController.Controller;
import pCloudyController.Helper.Base;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.qameta.allure.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

public class TestCase1_WelcomeNudge extends ExtentReport {
    //public static AndroidDriver driver;
    Controller controller = new Controller();
    @BeforeTest
    public void open_app() throws Exception {
        controller.prepareTest();
        driver = Controller.driver;
        Thread.sleep(3000);
        //test.log(LogStatus.PASS,"app is opened");

    }
    @Test(priority = 1)
    @Description("Start A Journey for Announcement nudges")
    @Severity(SeverityLevel.BLOCKER)
    public void startJourney() throws IOException, InterruptedException {
        AddNudgeJourney addNudgeJourney = new AddNudgeJourney();
        addNudgeJourney.addNudgeJourney("src/test/jsonFile/Ids.properties", "src/test/jsonFile/announcementNudgeJourney.json", "src/test/jsonFile/createAnnouncementJourney.json");
        Thread.sleep(3000);
        ActiveJourney activeJourney = new ActiveJourney();
        activeJourney.activateJourney();
        Thread.sleep(3000);
    }
    @Test(priority = 2)
    @Description("Save custom Event")
    @Severity(SeverityLevel.MINOR)
    public void saveCustomEvent() throws Exception {
            WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
            //Select pop up
            welcomeNudge.envScrollMethod();
            Thread.sleep(1000);
            welcomeNudge.scrollToAnElementByText(driver, "India_Server");
            Thread.sleep(1000);
            welcomeNudge.appNameSelect();
            Thread.sleep(1000);
            welcomeNudge.popUpSelect();
            Thread.sleep(1000);
            welcomeNudge.resetApp();
            Thread.sleep(3000);
            //welcomeNudge.launchApp();
            //Second app launch
            welcomeNudge.envScrollMethod();
            Thread.sleep(3000);
            welcomeNudge.scrollToAnElementByText(driver, "India_Server");
            Thread.sleep(3000);
            welcomeNudge.appNameSelect();
            Thread.sleep(3000);
            welcomeNudge.popUpSelect();
            welcomeNudge.terminateApp();
            welcomeNudge.launchApp();
            //wno.app_foreGround();
            //Select MiltonGreen App
            welcomeNudge.miltonGreenBtn_click();
            Thread.sleep(3000);
            //welcomeNudge.scrollAndClick("FIRE CUSTOM EVENT");
            welcomeNudge.clickFireCustomEvent();
            Thread.sleep(3000);
            welcomeNudge.selectVendorSpinner();
            Thread.sleep(3000);
            welcomeNudge.selectSmartTech();
            Thread.sleep(3000);
            welcomeNudge.enterEventName("Seg-Test");
            welcomeNudge.selectSavePncEvent();
            Thread.sleep(3000);
            welcomeNudge.pressBackButton();
            Thread.sleep(1000);
            welcomeNudge.scrollToAnElementByText(driver, "POINT AND CLICK");
            welcomeNudge.pointClickBtn_click();
            Thread.sleep(3000);

    }
    @Test(description = "Announcement 1 nudge testCase", priority = 3)
    @Description("Announcement 1 nudge testCase")
    @Severity(SeverityLevel.CRITICAL)
    public void welcomeNudge() throws InterruptedException, IOException {
        try {
            WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
            welcomeNudge.fireEventBtn_click();
            Thread.sleep(3000);
            welcomeNudge.getWelcomeLog();
            Thread.sleep(1000);
            NudgeMap nudgeMap = NudgeMap.label1Key("Announcement - 1");
            System.out.println(nudgeMap);
            Thread.sleep(3000);
            welcomeNudge.WelcomeNudgeScreenShot();
            Thread.sleep(3000);
            welcomeNudge.nudgeMatch();
            Thread.sleep(3000);
            welcomeNudge.welcomeText();
            Thread.sleep(1000);
            welcomeNudge.label2Text();
            Thread.sleep(1000);
            welcomeNudge.oKGotItBtnText();
            welcomeNudge.okGotItBtn_click();
            Thread.sleep(3000);

        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Test(priority = 4)
    @Description("Announcement-2 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement2() throws InterruptedException, IOException, ParseException {
        try {
        Announcement2NudgeObject announcement2 = new Announcement2NudgeObject(driver);
        announcement2.getAnnouncement2Log();
        Thread.sleep(3000);
        NudgeMap nudgeMap = NudgeMap.label1Key("Announcement - 2");
        Thread.sleep(3000);
        announcement2.announcement2NudgeScreenShot();
        Thread.sleep(3000);
        announcement2.nudgeMatch();
        Thread.sleep(3000);
        announcement2.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement2.getLabel1Text();
        Thread.sleep(3000);
        announcement2.getBtn1Text();
        Thread.sleep(3000);
        announcement2.clickBtn1();
        Thread.sleep(3000);
        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }

    }
    @Test(priority = 5)
    @Description("Announcement-3 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement3() throws InterruptedException, IOException, ParseException {
        try{
        Announcement3NudgeObject announcement3 = new Announcement3NudgeObject(driver);
        announcement3.getAnnouncement3Log();
        Thread.sleep(3000);
        NudgeMap nudgeMap = NudgeMap.label1Key("Announcement - 3");
        announcement3.announcement3NudgeScreenShot();
        Thread.sleep(3000);
        announcement3.nudgeMatch();
        announcement3.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement3.getAnnouncement3Log();
        Thread.sleep(3000);
        announcement3.getLabel0Text();
        Thread.sleep(3000);
        announcement3.getBtn1Text();
        Thread.sleep(3000);
        announcement3.clickBtn1();
        Thread.sleep(3000);
        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Test(priority = 6)
    @Description("Announcement-4 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement4() throws InterruptedException, IOException, ParseException {
        try {
        Announcement4NudgeObject announcement4 = new Announcement4NudgeObject(driver);
        announcement4.getAnnouncement4Log();
        Thread.sleep(3000);
        NudgeMap nudgeMap = NudgeMap.label1Key("Announcement - 4");
        announcement4.announcement4NudgeScreenShot();
        Thread.sleep(3000);
        announcement4.nudgeMatch();
        announcement4.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement4.getAnnouncement4Log();
        Thread.sleep(3000);
        announcement4.getLabel1Text();
        Thread.sleep(3000);
        announcement4.getBtn1Text();
        Thread.sleep(3000);
        announcement4.clickBtn1();
        Thread.sleep(3000);
        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }
    @Test(priority = 7)
    @Description("Announcement-5 nudge TestCase")
    @Severity(SeverityLevel.CRITICAL)
    public void announcement5() throws InterruptedException, IOException, ParseException {
        try{
        Announcement5NudgeObject announcement5 = new Announcement5NudgeObject(driver);
        announcement5.getAnnouncement5Log();
        Thread.sleep(3000);
        NudgeMap nudgeMap = NudgeMap.label1Key("Announcement - 5");
        announcement5.announcement5NudgeScreenShot();
        Thread.sleep(3000);
        announcement5.nudgeMatch();
        announcement5.getAnnouncement2Img();
        Thread.sleep(3000);
        announcement5.getAnnouncement5Log();
        Thread.sleep(3000);
        announcement5.getLabel1Text();
        Thread.sleep(3000);
        announcement5.getBtn1Text();
        Thread.sleep(3000);
        announcement5.clickBtn1();
        Thread.sleep(3000);
        }catch (Exception e){
            Allure.addAttachment(UUID.randomUUID().toString(), new ByteArrayInputStream(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES)));

        }
    }

    @Description("Delete Announcement nudge journey")
    @Severity(SeverityLevel.BLOCKER)
    @Test(description = "Delete Announcement nudge journey", priority = 8)
    public  void deleteJourney() throws Exception {
        DeactiveJourney deactiveJourney = new DeactiveJourney();
        deactiveJourney.deactivateJourney();
        Thread.sleep(2000);
        DeleteJourney deleteJourney = new DeleteJourney();
        deleteJourney.deleteJourney();

    }

}
