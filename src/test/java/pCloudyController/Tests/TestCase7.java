package pCloudyController.Tests;
import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import Utility.Set_Up;
import com.ssts.pcloudy.appium.PCloudyAppiumSession;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import pCloudyController.Controller;
import io.appium.java_client.android.AndroidDriver;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TestCase7 extends  ExtentReport{
    //PCloudyAppiumSession pCloudySession;
    Controller controller =  new Controller();
    @BeforeTest
    public void beforeTest() throws Exception {
        //pCloudySession = (PCloudyAppiumSession) controller.setUpSuite();
        controller.prepareTest();
        driver = Controller.driver;
        //controller.setUpSuite();
//       Set_Up su = new Set_Up();
//       su.setupAppium();
//       driver = Set_Up.driver;

    }

    @Test
    public void abc2() throws Exception {
            WelcomeNudgeObject welcomeNudge = new WelcomeNudgeObject(driver);
            //Select pop up
            welcomeNudge.envScrollMethod();
            Thread.sleep(1000);
            welcomeNudge.scrollToAnElementByText(driver, "India_Server");
            Thread.sleep(1000);
            welcomeNudge.appNameSelect();
            Thread.sleep(1000);
            welcomeNudge.popUpSelect();
            Thread.sleep(1000);
            welcomeNudge.resetApp();
            Thread.sleep(3000);
            //welcomeNudge.launchApp();
            //Second app launch
            welcomeNudge.envScrollMethod();
            Thread.sleep(3000);
            welcomeNudge.scrollToAnElementByText(driver, "India_Server");
            Thread.sleep(3000);
            welcomeNudge.appNameSelect();
            Thread.sleep(3000);
            welcomeNudge.popUpSelect();
            welcomeNudge.terminateApp();
            welcomeNudge.launchApp();
            //wno.app_foreGround();
            //Select MiltonGreen App
            welcomeNudge.miltonGreenBtn_click();
            Thread.sleep(3000);
            //welcomeNudge.scrollAndClick("FIRE CUSTOM EVENT");
            welcomeNudge.clickFireCustomEvent();
            welcomeNudge.selectVendorSpinner();
            welcomeNudge.selectSmartTech();
            welcomeNudge.enterEventName("Seg-Test");
            welcomeNudge.selectSavePncEvent();
            welcomeNudge.pressBackButton();
            Thread.sleep(1000);
            welcomeNudge.scrollToAnElementByText(driver, "POINT AND CLICK");
            welcomeNudge.pointClickBtn_click();
            Thread.sleep(3000);

    }

    @AfterTest
    public void tearDown() throws Exception {
       // driver.quit();
        //controller.endTest();
        controller.afterTest();
//        pCloudySession.releaseSessionNow();
//        pCloudySession.getConnector().revokeTokenPrivileges(pCloudySession.getAuthToken());
    }
}
