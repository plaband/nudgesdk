package pCloudyController;

import PageObject.WelcomeNudgeObject;
import Utility.ExtentReport;
import com.ssts.pcloudy.Connector;
import com.ssts.pcloudy.Version;
import com.ssts.pcloudy.appium.PCloudyAppiumSession;
import com.ssts.pcloudy.dto.appium.booking.BookingDtoDevice;
import com.ssts.pcloudy.dto.device.MobileDevice;
import com.ssts.pcloudy.dto.file.PDriveFileDTO;
import com.ssts.pcloudy.exception.ConnectError;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

public class Controller extends ExtentReport{

    PCloudyAppiumSession pCloudySession;
    public static AndroidDriver driver;
    int deviceBookDuration = 30;
    Boolean autoSelectDevices = true;
    Connector con = new Connector("https://device.pcloudy.com/api/");
    @BeforeTest
    public Object setUpSuite() throws Exception {
        Connector con = new Connector("https://device.pcloudy.com/api/");
        // User Authentication over pCloudy
        String authToken = con.authenticateUser("plaban.dwivedy@netcore.co.in", "kfznxt93m3j3dtrs5jgqgpyk");
        // Select apk in pCloudy Cloud Drive
        File fileToBeUploaded = new File("./miltongreen-nonproguarded-userjourney.apk");
        PDriveFileDTO alreadyUploadedApp = con.getAvailableAppIfUploaded(authToken, fileToBeUploaded.getName());
        if (alreadyUploadedApp == null) {
            System.out.println("Uploading App: " + fileToBeUploaded.getAbsolutePath());
            PDriveFileDTO uploadedApp = con.uploadApp(authToken, fileToBeUploaded, false);
            System.out.println("App uploaded");
            alreadyUploadedApp = new PDriveFileDTO();
            alreadyUploadedApp.file = uploadedApp.file;
        } else {
            System.out.println("App already present. Not uploading... ");
        }

        ArrayList<MobileDevice> selectedDevices = new ArrayList<>();
        if (autoSelectDevices)
            selectedDevices.addAll(con.chooseDevices(authToken, "android", new Version("7.*.*"), new Version("8.*.*"), 1));
        else
            selectedDevices.add(con.chooseSingleDevice(authToken, "android"));

        // Book the selected devices in pCloudy
        String sessionName = "Appium Session " + new Date();
        BookingDtoDevice bookedDevice = con.AppiumApis().bookDevicesForAppium(authToken, selectedDevices, deviceBookDuration, sessionName)[0];
        System.out.println("Devices booked successfully");

        con.AppiumApis().initAppiumHubForApp(authToken, alreadyUploadedApp);

        pCloudySession = new PCloudyAppiumSession(con, authToken, bookedDevice);
        return pCloudySession;
    }
        @BeforeMethod
        public void prepareTest() throws Exception {
            pCloudySession = (PCloudyAppiumSession) setUpSuite();
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("newCommandTimeout", 600);
            capabilities.setCapability("launchTimeout", 150000);
            capabilities.setCapability("deviceName", pCloudySession.getDto().capabilities.deviceName);
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("appPackage", "io.hansel.sampleapp.userjourney.miltongreen");
            capabilities.setCapability("automationName", "uiautomator2");
            //caps.setCapability("appActivity","io.visualizerapp.app.MainActivity");
            capabilities.setCapability("appWaitActivity", "com.company.miltongreen.activity.LaunchActivity");
            capabilities.setCapability("noReset", true);

            URL appiumEndpoint = pCloudySession.getConnector().AppiumApis().getAppiumEndpoint(pCloudySession.getAuthToken());
            driver = new AndroidDriver(appiumEndpoint, capabilities);
        }

    @AfterMethod
    public void endTest() throws ConnectError, IOException {

        driver.quit();
    }
    @AfterTest
    public void afterTest() throws Exception {

        pCloudySession.releaseSessionNow();
        pCloudySession.getConnector().revokeTokenPrivileges(pCloudySession.getAuthToken());
    }



}

